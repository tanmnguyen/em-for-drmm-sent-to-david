import cPickle as pickle

import numpy as np
import theano
from theano import tensor as T
from theano.tensor.shared_randomstreams import RandomStreams
from nn_functions_latest import LogisticRegression, LogisticRegressionForSemisupervised, SoftmaxNonlinearity, SoftmaxNonlinearitySemisupervised, HiddenLayerInSoftmax, ConvLayerInSoftmax

from CRM_no_factor_latest import CRM
from DRM_no_factor_latest import DRM_model

import time


class MNIST_Conv_Small_5_Layers(DRM_model):
    '''
    Conv_Small that the Ladder Network uses for MNIST

    `batch_size`:   size of minibatches used in each iteration
    `Cin`: The number of channels of the input
    `W`: The width of the input
    `H`: The height of the input
    `seed`: random seed
    `param_dir`: dir that saved the trained DRMM. Used in transfer learning or just to continue the training that stopped
                 for some reason
    `train_mode`: supervised, semisupervised, unsupervised
    `reconst_weights`: used to weigh reconstruction cost at each layer in the DRMM
    `xavier_init`: {True, False} use Xavier initialization or not
    `grad_min, grad_max`: clip the gradients
    `init_params`: {True, False} if True, load parameters from a trained DRMM
    `denoising`: {simple, message-passing} denoising mode
    `noise_std`: standard deviation of the noise added to the model
    `is_bn_BU`: {True, False} use batch normalization in the Bottom-Up or not
    `is_bn_TD`: {True, False} use batch normalization in the Top-Down or not
    `top_down_mode`: {signed, normal} use signed DRMM or NN-DRMM
    `is_TD_relu`: {True, False} use ReLU in the TopDown or not
    `nonlin`: {relu, abs} the nonlinearity to use in the Bottom-Up
    `is_tied_bn`: {True, False} if True, tie Batch Norm in the Bottom-Up and Top-Down
    `is_Dg`: {True, False} if True, use matrix preconditioning
    `method`: {SGD, adam} training method
    `KL_coef`: weight of the KL divergence cost
    `sign_cost_weight`: weight of hte sign cost
    `is_reluI`: {True, False} if True, apply ReLU on the reconstructed image
    `is_end_to_end`: {True, False} if True, train end-to-end
    `is_sample_c`: {True, False} if True, do sampling during reconstruction
    `is_one_hot`: {True, False} if True, turn c_hat into a one hot encoding vector and use it to reconstruct the image
    `num_TD_samplings`: number of samples used in the Top-Down reconstruction
    `KL_t_coef`: weight of the KL divergence on t cost. We are not using it now.
    `is_pruning`: do pruning or not
    `prun_threshold`: threshold used to prun the connections
    '''
    # TODO: factor out common init code from all models
    # TODO: make noise_weights parameters of the model to handle the noisy case duing testing and validation
    def __init__(self, batch_size, Cin, W, H, seed, param_dir=[], train_mode='supervised',
                 reconst_weights=[0.0, 0.0, 0.0, 0.0, 0.0], xavier_init=False, grad_min=-np.inf, grad_max=np.inf,
                 init_params=False, denoising='simple', noise_std=0.45, noise_weights=[0.0, 0.0, 0.0, 0.0, 0.0],
                 is_bn_BU=False, is_bn_TD=False, top_down_mode=None, is_TD_relu=False, nonlin='relu', is_tied_bn=False,
                 is_Dg=False, method='SGD', KL_coef=0.0, sign_cost_weight=0.0, is_reluI=False, is_end_to_end=False,
                 is_sample_c=False, is_one_hot=False, num_TD_samplings=1, KL_t_coef=0.0, is_prun=False, prun_threshold=0.5):

        print('Your model is MNIST_Conv_Small_5_Layers')

        self.noise_std = noise_std
        self.noise_weights = noise_weights
        self.reconst_weights = reconst_weights
        self.sign_cost_weight = sign_cost_weight

        self.is_bn_BU = is_bn_BU
        self.is_bn_TD = is_bn_TD
        self.is_tied_bn = is_tied_bn
        self.is_Dg = is_Dg
        self.is_end_to_end = is_end_to_end
        self.is_sample_c = is_sample_c
        self.is_one_hot = is_one_hot
        self.num_TD_samplings = num_TD_samplings

        self.batch_size = batch_size
        self.train_mode = train_mode
        self.denoising = denoising
        self.grad_min = grad_min
        self.grad_max = grad_max

        self.top_down_mode = top_down_mode
        self.is_TD_relu = is_TD_relu
        self.is_reluI = is_reluI
        self.nonlin = nonlin
        self.KL_coef = KL_coef
        self.method = method
        self.KL_t_coef = KL_t_coef
        self.is_prun = is_prun
        self.prun_threshold = prun_threshold

        is_noisy = [False] * len(self.noise_weights)
        for i in xrange(len(is_noisy)):
            if np.sum(self.noise_weights[0:i + 1]) > 0.0:
                is_noisy[i] = True

        print('noise_weights is:')
        print(self.noise_weights)
        print('is_noisy is:')
        print(is_noisy)

        # Initialize params
        if init_params:
            print('Initialize params using the params stored at:')
            print(param_dir)
            pkl_file = open(param_dir, 'rb')
            params = pickle.load(pkl_file)
            lambdas_val_init = params['lambdas_val']
            amps_val_init = params['amps_val']
            pkl_file.close()
        else:
            print('Randomly initialize params')
            lambdas_val_init = [None] * len(self.noise_weights)
            amps_val_init = [None] * len(self.noise_weights)

        # Build the model

        self.H1 = H  # 28
        self.W1 = W  # 28
        self.Cin1 = Cin  # 1

        self.h1 = 5
        self.w1 = 5
        self.K1 = 32
        self.M1 = 1

        self.H2 = (self.H1 + self.h1 - 1) / 2  # 16
        self.W2 = (self.W1 + self.w1 - 1) / 2  # 16
        self.Cin2 = self.K1  # 32

        self.h2 = 3
        self.w2 = 3
        self.K2 = 64
        self.M2 = 1

        self.H3 = self.H2 - self.h2 + 1  # 14
        self.W3 = self.W2 - self.w2 + 1  # 14
        self.Cin3 = self.K2  # 64

        self.h3 = 3
        self.w3 = 3
        self.K3 = 64
        self.M3 = 1

        self.H4 = (self.H3 + self.h3 - 1) / 2  # 8
        self.W4 = (self.W3 + self.w3 - 1) / 2  # 8
        self.Cin4 = self.K3  # 64

        self.h4 = 3
        self.w4 = 3
        self.K4 = 128
        self.M4 = 1

        self.H5 = self.H4 - self.h4 + 1  # 6
        self.W5 = self.W4 - self.w4 + 1  # 6
        self.Cin5 = self.K4  # 128

        self.h5 = 1
        self.w5 = 1
        self.K5 = 10
        self.M5 = 1

        self.H_Softmax = 1
        self.W_Softmax = 1
        self.Cin_Softmax = self.K5  # 10

        self.h_Softmax = 1
        self.w_Softmax = 1
        self.K_Softmax = 10
        self.M_Softmax = 1

        self.seed = seed
        np.random.seed(self.seed)


        if train_mode == 'supervised':
            self.x_lab = T.tensor4('x_lab')
            self.x_unl = None
        elif train_mode == 'unsupervised':
            self.x_lab = None
            self.x_unl = T.tensor4('x_unl')
        else:
            self.x_lab = T.tensor4('x_lab')
            self.x_unl = T.tensor4('x_unl')

        self.y_lab = T.ivector('y_lab')
        self.lr = T.scalar('l_r', dtype=theano.config.floatX)
        self.is_train = T.iscalar('is_train')
        self.momentum_bn = T.scalar('momentum_bn', dtype=theano.config.floatX)

        # Forward Step
        self.conv1 = CRM(
            data_4D_lab=self.x_lab,
            data_4D_unl=self.x_unl,
            data_4D_unl_clean=self.x_unl,
            noise_weight=self.noise_weights[0],
            noise_std=self.noise_std,
            is_train = self.is_train, momentum_bn=self.momentum_bn,
            K=self.K1, M=self.M1, W=self.W1, H=self.H1,
            w=self.w1, h=self.h1, Cin=self.Cin1, Ni=self.batch_size,
            amps_val_init=amps_val_init[4], lambdas_val_init=lambdas_val_init[4],
            pool_t_mode='max_t',
            border_mode='full',
            max_condition_number=1.e3, xavier_init=xavier_init,
            is_noisy=is_noisy[0], is_bn_BU=self.is_bn_BU, is_bn_TD=self.is_bn_TD, is_tied_bn=self.is_tied_bn, nonlin=self.nonlin,
            is_Dg = self.is_Dg,
            is_prun=self.is_prun)

        self.conv1._E_step_Bottom_Up()

        self.conv2 = CRM(data_4D_lab=self.conv1.output_lab,
                         data_4D_unl=self.conv1.output,
                         data_4D_unl_clean=self.conv1.output_clean,
                         noise_weight=self.noise_weights[1],
                         noise_std=self.noise_std,
                         is_train = self.is_train, momentum_bn=self.momentum_bn,
                         K=self.K2, M=self.M2, W=self.W2, H=self.H2,
                         w=self.w2, h=self.h2, Cin=self.Cin2, Ni=self.batch_size,
                         amps_val_init=amps_val_init[3], lambdas_val_init=lambdas_val_init[3],
                         pool_t_mode=None,
                         border_mode='valid',
                         max_condition_number=1.e3, xavier_init=xavier_init,
                         is_noisy=is_noisy[1], is_bn_BU=self.is_bn_BU, is_bn_TD=self.is_bn_TD,
                         is_tied_bn=self.is_tied_bn, nonlin=self.nonlin,
                         is_Dg=self.is_Dg,
                         is_prun=self.is_prun)

        self.conv2._E_step_Bottom_Up()

        self.conv3 = CRM(data_4D_lab=self.conv2.output_lab,
                         data_4D_unl=self.conv2.output,
                         data_4D_unl_clean=self.conv2.output_clean,
                         noise_weight=self.noise_weights[2],
                         noise_std=self.noise_std,
                         is_train = self.is_train, momentum_bn=self.momentum_bn,
                         K=self.K3, M=self.M3, W=self.W3, H=self.H3,
                         w=self.w3, h=self.h3, Cin=self.Cin3, Ni=self.batch_size,
                         amps_val_init=amps_val_init[2], lambdas_val_init=lambdas_val_init[2],
                         pool_t_mode='max_t',
                         border_mode='full',
                         max_condition_number=1.e3, xavier_init=xavier_init,
                         is_noisy=is_noisy[2], is_bn_BU=self.is_bn_BU, is_bn_TD=self.is_bn_TD,
                         is_tied_bn=self.is_tied_bn, nonlin=self.nonlin,
                         is_Dg=self.is_Dg,
                         is_prun=self.is_prun)

        self.conv3._E_step_Bottom_Up()

        self.conv4 = CRM(data_4D_lab=self.conv3.output_lab,
                         data_4D_unl=self.conv3.output,
                         data_4D_unl_clean=self.conv3.output_clean,
                         noise_weight=self.noise_weights[3],
                         noise_std=self.noise_std,
                         is_train = self.is_train, momentum_bn=self.momentum_bn,
                         K=self.K4, M=self.M4, W=self.W4, H=self.H4,
                         w=self.w4, h=self.h4, Cin=self.Cin4, Ni=self.batch_size,
                         amps_val_init=amps_val_init[1], lambdas_val_init=lambdas_val_init[1],
                         pool_t_mode=None,
                         border_mode='valid',
                         max_condition_number=1.e3, xavier_init=xavier_init,
                         is_noisy=is_noisy[3], is_bn_BU=self.is_bn_BU, is_bn_TD=self.is_bn_TD,
                         is_tied_bn=self.is_tied_bn, nonlin=self.nonlin,
                         is_Dg=self.is_Dg,
                         is_prun=self.is_prun)

        self.conv4._E_step_Bottom_Up()

        self.conv5 = CRM(data_4D_lab=self.conv4.output_lab,
                         data_4D_unl=self.conv4.output,
                         data_4D_unl_clean=self.conv4.output_clean,
                         noise_weight=self.noise_weights[4],
                         noise_std=self.noise_std,
                         is_train = self.is_train, momentum_bn=self.momentum_bn,
                         K=self.K5, M=self.M5, W=self.W5, H=self.H5,
                         w=self.w5, h=self.h5, Cin=self.Cin5, Ni=self.batch_size,
                         amps_val_init=amps_val_init[0], lambdas_val_init=lambdas_val_init[0],
                         pool_t_mode='mean_t',
                         border_mode='valid', mean_pool_size=(6, 6),
                         max_condition_number=1.e3, xavier_init=xavier_init,
                         is_noisy=is_noisy[4], is_bn_BU=self.is_bn_BU, is_bn_TD=self.is_bn_TD,
                         is_tied_bn=self.is_tied_bn, nonlin=self.nonlin,
                         is_Dg=self.is_Dg,
                         is_prun=self.is_prun)

        self.conv5._E_step_Bottom_Up()

        self.RegressionInSoftmax = HiddenLayerInSoftmax(input_lab=self.conv5.output_lab.flatten(2),
                                                        input_unl=self.conv5.output.flatten(2),
                                                        input_clean=self.conv5.output_clean.flatten(2),
                                                        n_in=self.Cin_Softmax, n_out=self.K_Softmax,
                                                        W_init=None, b_init=None)

        softmax_input_lab = self.RegressionInSoftmax.output_lab
        softmax_input = self.RegressionInSoftmax.output
        softmax_input_clean = self.RegressionInSoftmax.output_clean

        # classify the values of the fully-connected sigmoidal layer
        self.softmax_layer_nonlin = SoftmaxNonlinearity(input_lab=softmax_input_lab, input_unl=softmax_input,
                                                        input_clean=softmax_input_clean)

        # self.layers = [self.conv6, self.conv5, self.conv4, self.conv3, self.conv2, self.conv1]
        self.layers = [self.conv5, self.conv4, self.conv3, self.conv2, self.conv1]
        self.N_layer = len(self.layers)

        # build Top-Down pass
        if self.is_sample_c and (not self.is_one_hot):
            print('Do exact %i samplings'%self.num_TD_samplings)
            time.sleep(5)
            self.topk_posteriors = T.dot(-T.sort(-self.softmax_layer_nonlin.gammas, axis=1),
                                         T.eye(10, self.num_TD_samplings))

            one_hot_vec1, output1 = self.compute_one_hot(self.softmax_layer_nonlin.gammas)
            one_hot_vec2, output2 = self.compute_one_hot(output1)
            # output3 = self.compute_one_hot(output2)
            # output4 = self.compute_one_hot(output3)
            # output5 = self.compute_one_hot(output4)
            # output6 = self.compute_one_hot(output5)
            # output7 = self.compute_one_hot(output6)
            # output8 = self.compute_one_hot(output7)
            # output9 = self.compute_one_hot(output8)
            # output10 = self.compute_one_hot(output9)
            one_hot_vec = T.concatenate([one_hot_vec1, one_hot_vec2], axis=0)
            self.Sample_with_BottomUp_g(one_hot_vec=one_hot_vec, top_down_mode=self.top_down_mode,
                                        is_TD_relu=self.is_TD_relu, is_reluI=self.is_reluI)
        elif self.is_end_to_end:
            print('Include Softmax Regression in the Top Down')
            self.Build_TopDown_End_to_End(top_down_mode=self.top_down_mode, is_TD_relu=self.is_TD_relu,
                                          is_reluI=self.is_reluI, is_one_hot=self.is_one_hot)
        else:
            print('Not include Softmax Regression in the Top Down')
            self.Build_TopDown(top_down_mode=self.top_down_mode, is_TD_relu=self.is_TD_relu, is_reluI=self.is_reluI)

        # build the cost function for the model
        self.Build_Cost()

        # build update rules for the model
        self.Build_Update_Rule(method=self.method)

    def compute_one_hot(self, input):
        sampling_indx = T.argmax(input, axis=1)
        one_hot_vec = T.extra_ops.to_one_hot(sampling_indx, 10)
        output = input - input * one_hot_vec
        return one_hot_vec, output

########################################################################################################################
class CIFAR10_Conv_Large_9_Layers(DRM_model):
    '''
    Conv_Small that the Ladder Network uses for MNIST

    `batch_size`:   size of minibatches used in each iteration
    `Cin`: The number of channels of the input
    `W`: The width of the input
    `H`: The height of the input
    `seed`: random seed
    `param_dir`: dir that saved the trained DRMM. Used in transfer learning or just to continue the training that stopped
                 for some reason
    `train_mode`: supervised, semisupervised, unsupervised
    `reconst_weights`: used to weigh reconstruction cost at each layer in the DRMM
    `xavier_init`: {True, False} use Xavier initialization or not
    `grad_min, grad_max`: clip the gradients
    `init_params`: {True, False} if True, load parameters from a trained DRMM
    `denoising`: {simple, message-passing} denoising mode
    `noise_std`: standard deviation of the noise added to the model
    `is_bn_BU`: {True, False} use batch normalization in the Bottom-Up or not
    `is_bn_TD`: {True, False} use batch normalization in the Top-Down or not
    `top_down_mode`: {signed, normal} use signed DRMM or NN-DRMM
    `is_TD_relu`: {True, False} use ReLU in the TopDown or not
    `nonlin`: {relu, abs} the nonlinearity to use in the Bottom-Up
    `is_tied_bn`: {True, False} if True, tie Batch Norm in the Bottom-Up and Top-Down
    `is_Dg`: {True, False} if True, use matrix preconditioning
    `method`: {SGD, adam} training method
    `KL_coef`: weight of the KL divergence cost
    `sign_cost_weight`: weight of hte sign cost
    `is_reluI`: {True, False} if True, apply ReLU on the reconstructed image
    `is_end_to_end`: {True, False} if True, train end-to-end
    `is_sample_c`: {True, False} if True, do sampling during reconstruction
    `is_one_hot`: {True, False} if True, turn c_hat into a one hot encoding vector and use it to reconstruct the image
    `num_TD_samplings`: number of samples used in the Top-Down reconstruction
    `KL_t_coef`: weight of the KL divergence on t cost. We are not using it now.
    `is_pruning`: do pruning or not
    `prun_threshold`: threshold used to prun the connections
    '''
    # TODO: factor out common init code from all models
    def __init__(self, batch_size, Cin, W, H, seed, param_dir=[], train_mode='supervised',
                 reconst_weights=[0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], xavier_init=False, grad_min=-np.inf, grad_max=np.inf,
                 init_params=False, denoising='simple', noise_std=0.45, noise_weights=[0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                 is_bn_BU=False, is_bn_TD=False, top_down_mode=None, is_TD_relu=False, nonlin='relu',
                 is_tied_bn=False, is_Dg=False,
                 method='SGD', KL_coef=0.0, sign_cost_weight=0.0, is_reluI=False, is_end_to_end=False,
                 is_sample_c=False, is_one_hot=False, num_TD_samplings=1, KL_t_coef=0.0, is_prun=False, prun_threshold=0.5):

        print('Your model is CIFAR10_Conv_Large_9_Layers')

        self.noise_std = noise_std
        self.noise_weights = noise_weights
        self.reconst_weights = reconst_weights
        self.sign_cost_weight = sign_cost_weight

        self.is_bn_BU = is_bn_BU
        self.is_bn_TD = is_bn_TD
        self.is_tied_bn = is_tied_bn
        self.is_Dg = is_Dg
        self.is_end_to_end = is_end_to_end
        self.is_sample_c = is_sample_c
        self.is_one_hot = is_one_hot
        self.num_TD_samplings = num_TD_samplings

        self.batch_size = batch_size
        self.train_mode = train_mode
        self.denoising = denoising
        self.grad_min = grad_min
        self.grad_max = grad_max

        self.top_down_mode = top_down_mode
        self.is_TD_relu = is_TD_relu
        self.is_reluI = is_reluI
        self.nonlin = nonlin
        self.method = method
        self.KL_coef = KL_coef
        self.KL_t_coef = KL_t_coef
        self.is_prun = is_prun
        self.prun_threshold = prun_threshold

        is_noisy = [False] * len(self.noise_weights)
        for i in xrange(len(is_noisy)):
            if np.sum(self.noise_weights[0:i + 1]) > 0.0:
                is_noisy[i] = True

        print('noise_weights is:')
        print(self.noise_weights)
        print('is_noisy is:')
        print(is_noisy)

        # Initialize params
        if init_params:
            print('Initialize params using the params stored at:')
            print(param_dir)
            pkl_file = open(param_dir, 'rb')
            params = pickle.load(pkl_file)
            lambdas_val_init = params['lambdas_val']
            amps_val_init = params['amps_val']
            pkl_file.close()
        else:
            print('Randomly initialize params')
            lambdas_val_init = [None] * len(self.noise_weights)
            amps_val_init = [None] * len(self.noise_weights)

        # Build the model

        self.H1 = H  # 32
        self.W1 = W  # 32
        self.Cin1 = Cin  # 3

        self.h1 = 3
        self.w1 = 3
        self.K1 = 96
        self.M1 = 1

        self.H2 = self.H1  # 32
        self.W2 = self.W1  # 32
        self.Cin2 = self.K1  # 96

        self.h2 = 3
        self.w2 = 3
        self.K2 = 96
        self.M2 = 1

        self.H3 = self.H2 + self.h2 - 1  # 34
        self.W3 = self.W2 + self.w2 - 1  # 34
        self.Cin3 = self.K2  # 96

        self.h3 = 3
        self.w3 = 3
        self.K3 = 96
        self.M3 = 1

        self.H4 = (self.H3 + self.h3 - 1) / 2  # 18
        self.W4 = (self.W3 + self.w3 - 1) / 2  # 18
        self.Cin4 = self.K3  # 96

        self.h4 = 3
        self.w4 = 3
        self.K4 = 192
        self.M4 = 1

        self.H5 = self.H4 - self.h4 + 1  # 16
        self.W5 = self.W4 - self.w4 + 1  # 16
        self.Cin5 = self.K4  # 192

        self.h5 = 3
        self.w5 = 3
        self.K5 = 192
        self.M5 = 1

        self.H6 = self.H5 + self.h5 - 1  # 18
        self.W6 = self.W5 + self.w5 - 1  # 18
        self.Cin6 = self.K5  # 192

        self.h6 = 3
        self.w6 = 3
        self.K6 = 192
        self.M6 = 1

        self.H7 = (self.H6 - self.h6 + 1) / 2  # 8
        self.W7 = (self.W6 - self.w6 + 1) / 2  # 8
        self.Cin7 = self.K6  # 192

        self.h7 = 3
        self.w7 = 3
        self.K7 = 192
        self.M7 = 1

        self.H8 = self.H7 - self.h7 + 1  # 6
        self.W8 = self.W7 - self.w7 + 1  # 6
        self.Cin8 = self.K7  # 192

        self.h8 = 1
        self.w8 = 1
        self.K8 = 192
        self.M8 = 1

        self.H9 = self.H8 - self.h8 + 1  # 6
        self.W9 = self.W8 - self.w8 + 1  # 6
        self.Cin9 = self.K8  # 192

        self.h9 = 1
        self.w9 = 1
        self.K9 = 10
        self.M9 = 1

        self.H_Softmax = 1
        self.W_Softmax = 1
        self.Cin_Softmax = self.K9  # 10

        self.h_Softmax = 1
        self.w_Softmax = 1
        self.K_Softmax = 10
        self.M_Softmax = 1

        self.seed = seed
        np.random.seed(self.seed)

        if train_mode == 'supervised':
            self.x_lab = T.tensor4('x_lab')
            self.x_unl = None
        elif train_mode == 'unsupervised':
            self.x_lab = None
            self.x_unl = T.tensor4('x_unl')
        else:
            self.x_lab = T.tensor4('x_lab')
            self.x_unl = T.tensor4('x_unl')

        self.y_lab = T.ivector('y_lab')
        self.lr = T.scalar('l_r', dtype=theano.config.floatX)
        self.is_train = T.iscalar('is_train')
        self.momentum_bn = T.scalar('momentum_bn', dtype=theano.config.floatX)

        # Forward Step
        self.conv1 = CRM(
            data_4D_lab=self.x_lab,
            data_4D_unl=self.x_unl,
            data_4D_unl_clean=self.x_unl,
            noise_weight=self.noise_weights[0],
            noise_std=self.noise_std,
            is_train=self.is_train, momentum_bn=self.momentum_bn,
            K=self.K1, M=self.M1, W=self.W1, H=self.H1,
            w=self.w1, h=self.h1, Cin=self.Cin1, Ni=self.batch_size,
            amps_val_init=amps_val_init[8], lambdas_val_init=lambdas_val_init[8],
            pool_t_mode=None,
            border_mode='half',
            max_condition_number=1.e3, xavier_init=xavier_init,
            is_noisy=is_noisy[0], is_bn_BU=self.is_bn_BU, is_bn_TD=self.is_bn_TD, is_tied_bn=self.is_tied_bn,
            nonlin=self.nonlin,
            is_Dg=self.is_Dg,
            is_prun=self.is_prun)

        self.conv1._E_step_Bottom_Up()

        self.conv2 = CRM(data_4D_lab=self.conv1.output_lab,
                         data_4D_unl=self.conv1.output,
                         data_4D_unl_clean=self.conv1.output_clean,
                         noise_weight=self.noise_weights[1],
                         noise_std=self.noise_std,
                         is_train=self.is_train, momentum_bn=self.momentum_bn,
                         K=self.K2, M=self.M2, W=self.W2, H=self.H2,
                         w=self.w2, h=self.h2, Cin=self.Cin2, Ni=self.batch_size,
                         amps_val_init=amps_val_init[7], lambdas_val_init=lambdas_val_init[7],
                         pool_t_mode=None,
                         border_mode='full',
                         max_condition_number=1.e3, xavier_init=xavier_init,
                         is_noisy=is_noisy[1], is_bn_BU=self.is_bn_BU, is_bn_TD=self.is_bn_TD,
                         is_tied_bn=self.is_tied_bn, nonlin=self.nonlin,
                         is_Dg=self.is_Dg,
                         is_prun=self.is_prun)

        self.conv2._E_step_Bottom_Up()

        self.conv3 = CRM(data_4D_lab=self.conv2.output_lab,
                         data_4D_unl=self.conv2.output,
                         data_4D_unl_clean=self.conv2.output_clean,
                         noise_weight=self.noise_weights[2],
                         noise_std=self.noise_std,
                         is_train=self.is_train, momentum_bn=self.momentum_bn,
                         K=self.K3, M=self.M3, W=self.W3, H=self.H3,
                         w=self.w3, h=self.h3, Cin=self.Cin3, Ni=self.batch_size,
                         amps_val_init=amps_val_init[6], lambdas_val_init=lambdas_val_init[6],
                         pool_t_mode='max_t',
                         border_mode='full',
                         max_condition_number=1.e3, xavier_init=xavier_init,
                         is_noisy=is_noisy[2], is_bn_BU=self.is_bn_BU, is_bn_TD=self.is_bn_TD,
                         is_tied_bn=self.is_tied_bn, nonlin=self.nonlin,
                         is_Dg=self.is_Dg,
                         is_prun=self.is_prun)

        self.conv3._E_step_Bottom_Up()

        self.conv4 = CRM(data_4D_lab=self.conv3.output_lab,
                         data_4D_unl=self.conv3.output,
                         data_4D_unl_clean=self.conv3.output_clean,
                         noise_weight=self.noise_weights[3],
                         noise_std=self.noise_std,
                         is_train=self.is_train, momentum_bn=self.momentum_bn,
                         K=self.K4, M=self.M4, W=self.W4, H=self.H4,
                         w=self.w4, h=self.h4, Cin=self.Cin4, Ni=self.batch_size,
                         amps_val_init=amps_val_init[5], lambdas_val_init=lambdas_val_init[5],
                         pool_t_mode=None,
                         border_mode='valid',
                         max_condition_number=1.e3, xavier_init=xavier_init,
                         is_noisy=is_noisy[3], is_bn_BU=self.is_bn_BU, is_bn_TD=self.is_bn_TD,
                         is_tied_bn=self.is_tied_bn, nonlin=self.nonlin,
                         is_Dg=self.is_Dg,
                         is_prun=self.is_prun)

        self.conv4._E_step_Bottom_Up()

        self.conv5 = CRM(data_4D_lab=self.conv4.output_lab,
                         data_4D_unl=self.conv4.output,
                         data_4D_unl_clean=self.conv4.output_clean,
                         noise_weight=self.noise_weights[4],
                         noise_std=self.noise_std,
                         is_train=self.is_train, momentum_bn=self.momentum_bn,
                         K=self.K5, M=self.M5, W=self.W5, H=self.H5,
                         w=self.w5, h=self.h5, Cin=self.Cin5, Ni=self.batch_size,
                         amps_val_init=amps_val_init[4], lambdas_val_init=lambdas_val_init[4],
                         pool_t_mode=None,
                         border_mode='full',
                         max_condition_number=1.e3, xavier_init=xavier_init,
                         is_noisy=is_noisy[4], is_bn_BU=self.is_bn_BU, is_bn_TD=self.is_bn_TD,
                         is_tied_bn=self.is_tied_bn, nonlin=self.nonlin,
                         is_Dg=self.is_Dg,
                         is_prun=self.is_prun)

        self.conv5._E_step_Bottom_Up()

        self.conv6 = CRM(data_4D_lab=self.conv5.output_lab,
                         data_4D_unl=self.conv5.output,
                         data_4D_unl_clean=self.conv5.output_clean,
                         noise_weight=self.noise_weights[5],
                         noise_std=self.noise_std,
                         is_train=self.is_train, momentum_bn=self.momentum_bn,
                         K=self.K6, M=self.M6, W=self.W6, H=self.H6,
                         w=self.w6, h=self.h6, Cin=self.Cin6, Ni=self.batch_size,
                         amps_val_init=amps_val_init[3], lambdas_val_init=lambdas_val_init[3],
                         pool_t_mode='max_t',
                         border_mode='valid',
                         max_condition_number=1.e3, xavier_init=xavier_init,
                         is_noisy=is_noisy[5], is_bn_BU=self.is_bn_BU, is_bn_TD=self.is_bn_TD,
                         is_tied_bn=self.is_tied_bn, nonlin=self.nonlin,
                         is_Dg=self.is_Dg,
                         is_prun=self.is_prun)

        self.conv6._E_step_Bottom_Up()

        self.conv7 = CRM(data_4D_lab=self.conv6.output_lab,
                         data_4D_unl=self.conv6.output,
                         data_4D_unl_clean=self.conv6.output_clean,
                         noise_weight=self.noise_weights[6],
                         noise_std=self.noise_std,
                         is_train=self.is_train, momentum_bn=self.momentum_bn,
                         K=self.K7, M=self.M7, W=self.W7, H=self.H7,
                         w=self.w7, h=self.h7, Cin=self.Cin7, Ni=self.batch_size,
                         amps_val_init=amps_val_init[2], lambdas_val_init=lambdas_val_init[2],
                         pool_t_mode=None,
                         border_mode='valid',
                         max_condition_number=1.e3, xavier_init=xavier_init,
                         is_noisy=is_noisy[6], is_bn_BU=self.is_bn_BU, is_bn_TD=self.is_bn_TD,
                         is_tied_bn=self.is_tied_bn, nonlin=self.nonlin,
                         is_Dg=self.is_Dg,
                         is_prun=self.is_prun)

        self.conv7._E_step_Bottom_Up()

        self.conv8 = CRM(data_4D_lab=self.conv7.output_lab,
                         data_4D_unl=self.conv7.output,
                         data_4D_unl_clean=self.conv7.output_clean,
                         noise_weight=self.noise_weights[7],
                         noise_std=self.noise_std,
                         is_train=self.is_train, momentum_bn=self.momentum_bn,
                         K=self.K8, M=self.M8, W=self.W8, H=self.H8,
                         w=self.w8, h=self.h8, Cin=self.Cin8, Ni=self.batch_size,
                         amps_val_init=amps_val_init[1], lambdas_val_init=lambdas_val_init[1],
                         pool_t_mode=None,
                         border_mode='valid',
                         max_condition_number=1.e3, xavier_init=xavier_init,
                         is_noisy=is_noisy[7], is_bn_BU=self.is_bn_BU, is_bn_TD=self.is_bn_TD,
                         is_tied_bn=self.is_tied_bn, nonlin=self.nonlin,
                         is_Dg=self.is_Dg,
                         is_prun=self.is_prun)

        self.conv8._E_step_Bottom_Up()

        self.conv9 = CRM(data_4D_lab=self.conv8.output_lab,
                         data_4D_unl=self.conv8.output,
                         data_4D_unl_clean=self.conv8.output_clean,
                         noise_weight=self.noise_weights[8],
                         noise_std=self.noise_std,
                         is_train=self.is_train, momentum_bn=self.momentum_bn,
                         K=self.K9, M=self.M9, W=self.W9, H=self.H9,
                         w=self.w9, h=self.h9, Cin=self.Cin9, Ni=self.batch_size,
                         amps_val_init=amps_val_init[0], lambdas_val_init=lambdas_val_init[0],
                         pool_t_mode='mean_t', mean_pool_size=(6,6),
                         border_mode='valid',
                         max_condition_number=1.e3, xavier_init=xavier_init,
                         is_noisy=is_noisy[8], is_bn_BU=self.is_bn_BU, is_bn_TD=self.is_bn_TD,
                         is_tied_bn=self.is_tied_bn, nonlin=self.nonlin,
                         is_Dg=self.is_Dg,
                         is_prun=self.is_prun)

        self.conv9._E_step_Bottom_Up()

        self.RegressionInSoftmax = HiddenLayerInSoftmax(input_lab=self.conv9.output_lab.flatten(2),
                                                        input_unl=self.conv9.output.flatten(2),
                                                        input_clean=self.conv9.output_clean.flatten(2),
                                                        n_in=self.Cin_Softmax, n_out=self.K_Softmax,
                                                        W_init=None, b_init=None)

        softmax_input_lab = self.RegressionInSoftmax.output_lab
        softmax_input = self.RegressionInSoftmax.output
        softmax_input_clean = self.RegressionInSoftmax.output_clean

        # classify the values of the fully-connected sigmoidal layer
        self.softmax_layer_nonlin = SoftmaxNonlinearity(input_lab=softmax_input_lab, input_unl=softmax_input,
                                                        input_clean=softmax_input_clean)

        # self.layers = [self.conv6, self.conv5, self.conv4, self.conv3, self.conv2, self.conv1]
        self.layers = [self.conv9, self.conv8, self.conv7, self.conv6, self.conv5, self.conv4, self.conv3, self.conv2, self.conv1]
        self.N_layer = len(self.layers)

        # build Top-Down pass
        if self.is_sample_c and (not self.is_one_hot):
            print('Do exact %i samplings' % self.num_TD_samplings)
            time.sleep(5)
            self.topk_posteriors = T.dot(-T.sort(-self.softmax_layer_nonlin.gammas, axis=1),
                                         T.eye(10, self.num_TD_samplings))

            one_hot_vec1, output1 = self.compute_one_hot(self.softmax_layer_nonlin.gammas)
            one_hot_vec2, output2 = self.compute_one_hot(output1)
            one_hot_vec3, output3 = self.compute_one_hot(output2)
            # output4 = self.compute_one_hot(output3)
            # output5 = self.compute_one_hot(output4)
            # output6 = self.compute_one_hot(output5)
            # output7 = self.compute_one_hot(output6)
            # output8 = self.compute_one_hot(output7)
            # output9 = self.compute_one_hot(output8)
            # output10 = self.compute_one_hot(output9)
            one_hot_vec = T.concatenate([one_hot_vec1, one_hot_vec2, one_hot_vec3], axis=0)
            self.Sample_with_BottomUp_g(one_hot_vec=one_hot_vec, top_down_mode=self.top_down_mode,
                                        is_TD_relu=self.is_TD_relu, is_reluI=self.is_reluI)
        elif self.is_end_to_end:
            print('Include Softmax Regression in the Top Down')
            self.Build_TopDown_End_to_End(top_down_mode=self.top_down_mode, is_TD_relu=self.is_TD_relu,
                                          is_reluI=self.is_reluI, is_one_hot=self.is_one_hot)
        else:
            print('Not include Softmax Regression in the Top Down')
            self.Build_TopDown(top_down_mode=self.top_down_mode, is_TD_relu=self.is_TD_relu, is_reluI=self.is_reluI)

        # build the cost function for the model
        self.Build_Cost()

        # build update rules for the model
        self.Build_Update_Rule(method=self.method)

    def compute_one_hot(self, input):
        sampling_indx = T.argmax(input, axis=1)
        one_hot_vec = T.extra_ops.to_one_hot(sampling_indx, 10)
        output = input - input * one_hot_vec
        return one_hot_vec, output
