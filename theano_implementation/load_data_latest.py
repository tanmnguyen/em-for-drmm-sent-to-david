import matplotlib as mpl
mpl.use('Agg')
from pylab import *

import gzip
import cPickle

import numpy as np

import random

import time

import theano as th

import os

import svhn_data

import cifar10_data


# from guppy import hpy; h=hpy()

def unpickle(file):
    # load pickle files
    import cPickle
    fo = open(file, 'rb')
    dict = cPickle.load(fo)
    fo.close()
    return dict

class DATA(object):
    """
    Class of DATA. Should be used to load data
    """
    def __init__(self, dataset_name, data_mode, data_dir, Nlabeled, Ni, Cin, H, W, seed, preprocess=False):
        self.dataset_name = dataset_name
        self.data_mode = data_mode
        self.data_dir = data_dir
        self.Cin = Cin
        self.Nlabeled = Nlabeled
        self.Ni= Ni
        self.H = H
        self.W = W
        self.seed = seed
        self.preprocess = preprocess

        # Set up random seed
        random.seed(self.seed)
        np.random.seed(self.seed)

        # load datasets according to their names
        if self.dataset_name == 'MNIST':
            self.load_mnist()
        elif self.dataset_name == 'SVHN':
            self.load_svhn()
        elif self.dataset_name == 'CIFAR10':
            self.load_cifar10(preprocess=self.preprocess)
        elif self.dataset_name == 'REVERSIBLE_CIFAR10':
            self.load_reversible_cifar10()
        elif self.dataset_name == 'REVERSIBLE_BLENDER_2AXIS':
            self.load_reversible_blender_2axis()
        else:
            self.load_general_data()

    def load_general_data(self):
        self.trainx_unl = []
        self.trainx_lab = []
        self.testx = []
        self.validx = []
        self.trainy_lab = []
        self.testy = []
        self.validy = []

    def load_mnist(self):
        # load MNIST data
        # Code based off: https://github.com/openai/improved-gan/blob/master/mnist_svhn_cifar10/train_mnist_feature_matching.py

        # load original data
        count = self.Nlabeled/10 # num labels of each class
        data_rng = np.random.RandomState(self.seed)
        data = np.load(self.data_dir)
        trainx = np.concatenate([data['x_train'], data['x_valid']], axis=0).astype(th.config.floatX)
        trainy = np.concatenate([data['y_train'], data['y_valid']]).astype(np.int32)
        self.trainx_unl = trainx.copy()
        self.testx = data['x_test'].astype(th.config.floatX)
        self.testy = data['y_test'].astype(np.int32)
        self.validx = data['x_valid'].astype(th.config.floatX) # validation set is now part of the training set
        self.validy = data['y_valid'].astype(np.int32)

        # select labeled data (This is redundant since we will select labels later during the training)
        inds = data_rng.permutation(trainx.shape[0])
        trainx = trainx[inds]
        trainy = trainy[inds]
        self.trainx_lab = []
        self.trainy_lab = []

        for j in range(10):
            self.trainx_lab.append(trainx[trainy == j][:count])
            self.trainy_lab.append(trainy[trainy == j][:count])

        self.trainx_lab = np.concatenate(self.trainx_lab, axis=0)
        self.trainy_lab = np.concatenate(self.trainy_lab, axis=0)

        # reshape the data into (N, C, W, H) format
        self.trainx_lab = np.reshape(self.trainx_lab[0:self.Nlabeled], newshape=(self.Nlabeled, self.Cin, self.H, self.W))
        self.trainx_unl = np.reshape(self.trainx_unl[0:self.Ni], newshape=(self.Ni, self.Cin, self.H, self.W))
        self.testx = np.reshape(self.testx, newshape=(np.shape(self.testx)[0], self.Cin, self.H, self.W))
        self.validx = np.reshape(self.validx, newshape=(np.shape(self.validx)[0], self.Cin, self.H, self.W))

        print('Check shape of data')
        print('Shape of trainx_lab'),
        print(np.shape(self.trainx_lab))
        print('Shape of trainx_unl'),
        print(np.shape(self.trainx_unl))
        print('Shape of testx'),
        print(np.shape(self.testx))
        print('Shape of validx'),
        print(np.shape(self.validx))
        print('Shape of trainy_lab'),
        print(np.shape(self.trainy_lab))
        print('Shape of testy'),
        print(np.shape(self.testy))
        print('Shape of validy'),
        print(np.shape(self.validy))

        time.sleep(5)

    def load_cifar10(self, preprocess=False):
        count = self.Nlabeled / 10  # num labels of each class
        data_rng = np.random.RandomState(self.seed)

        if preprocess == False: # load no-preprocessed data
            print('No Preprocessing on CIFAR10')
            trainx, trainy = cifar10_data.load(self.data_dir, subset='train')
            self.trainx_unl = trainx.copy()
            self.testx, self.testy = cifar10_data.load(self.data_dir, subset='test')
            self.validx = trainx[40000:50000].astype(th.config.floatX)
            self.validy = trainy[40000:50000].astype(np.int8)

            # select labeled data
            inds = data_rng.permutation(trainx.shape[0])
            trainx = trainx[inds]
            trainy = trainy[inds]
            self.trainx_lab = []
            self.trainy_lab = []
            for j in range(10):
                self.trainx_lab.append(trainx[trainy == j][:count])
                self.trainy_lab.append(trainy[trainy == j][:count])
            self.trainx_lab = np.concatenate(self.trainx_lab, axis=0)
            self.trainy_lab = np.concatenate(self.trainy_lab, axis=0)

            self.trainx_lab = self.trainx_lab[0:self.Nlabeled]
            self.trainx_unl = self.trainx_unl[0:self.Ni]

        else: # load preprocessed data
            print('Preprocessing on CIFAR10')
            train_set = np.load(os.path.join(self.data_dir, "pylearn2_gcn_whitened", "train.npy"))
            test_set = np.load(os.path.join(self.data_dir, "pylearn2_gcn_whitened", "test.npy"))

            train_set_1_temp = unpickle(self.data_dir + "/data_batch_1")
            train_set_2_temp = unpickle(self.data_dir + "/data_batch_2")
            train_set_3_temp = unpickle(self.data_dir + "/data_batch_3")
            train_set_4_temp = unpickle(self.data_dir + "/data_batch_4")
            train_set_5_temp = unpickle(self.data_dir + "/data_batch_5")
            test_set_temp = unpickle(self.data_dir + "/test_batch")
            train_set_labels = np.concatenate((train_set_1_temp['labels'], train_set_2_temp['labels'],
                                               train_set_3_temp['labels'], train_set_4_temp['labels'],
                                               train_set_5_temp['labels']), axis=0)
            test_set_labels = test_set_temp['labels']

            trainx = train_set.astype(th.config.floatX)
            trainy = np.asarray(train_set_labels, dtype=np.int32)
            self.trainx_unl = trainx.copy()
            self.testx = test_set.astype(th.config.floatX)
            self.testy = np.asarray(test_set_labels, dtype=np.int32)
            self.validx = train_set[40000:50000].astype(th.config.floatX)  # validation set is now part of the training set
            self.validy = np.asarray(train_set_labels[40000:50000], dtype=np.int32)

            # select labeled data
            inds = data_rng.permutation(trainx.shape[0])
            trainx = trainx[inds]
            trainy = trainy[inds]
            self.trainx_lab = []
            self.trainy_lab = []
            for j in range(10):
                self.trainx_lab.append(trainx[trainy == j][:count])
                self.trainy_lab.append(trainy[trainy == j][:count])
            self.trainx_lab = np.concatenate(self.trainx_lab, axis=0)
            self.trainy_lab = np.concatenate(self.trainy_lab, axis=0)

            self.trainx_lab = np.reshape(self.trainx_lab[0:self.Nlabeled],
                                         newshape=(self.Nlabeled, self.Cin, self.H, self.W))
            self.trainx_unl = np.reshape(self.trainx_unl[0:self.Ni], newshape=(self.Ni, self.Cin, self.H, self.W))
            self.testx = np.reshape(self.testx, newshape=(np.shape(self.testx)[0], self.Cin, self.H, self.W))
            self.validx = np.reshape(self.validx, newshape=(np.shape(self.validx)[0], self.Cin, self.H, self.W))

        # Check the shape of the data
        print('Check shape of data')
        print('Shape of trainx_lab'),
        print(np.shape(self.trainx_lab))
        print('Shape of trainx_unl'),
        print(np.shape(self.trainx_unl))
        print('Shape of testx'),
        print(np.shape(self.testx))
        print('Shape of validx'),
        print(np.shape(self.validx))
        print('Shape of trainy_lab'),
        print(np.shape(self.trainy_lab))
        print('Shape of testy'),
        print(np.shape(self.testy))
        print('Shape of validy'),
        print(np.shape(self.validy))

        print('Max pixel train')
        print(np.max(self.trainx_unl))
        print('Min pixel train')
        print(np.min(self.trainx_unl))

        print('Max pixel test')
        print(np.max(self.testx))
        print('Min pixel test')
        print(np.min(self.testx))

        time.sleep(10)

    def load_svhn(self):
        # Load SVHN
        # Code based off https://github.com/openai/improved-gan/blob/master/mnist_svhn_cifar10/train_svhn_minibatch_discrimination.py
        def rescale(mat): # rescale the data
            return np.transpose(np.cast[th.config.floatX]((-127.5 + mat) / 127.5), (3, 2, 0, 1))

        # load original datasets
        count = self.Nlabeled / 10  # num labels of each class
        data_rng = np.random.RandomState(self.seed)

        trainx, trainy = svhn_data.load(self.data_dir, 'train')
        testx, testy = svhn_data.load(self.data_dir, 'test')
        trainx = rescale(trainx)
        testx = rescale(testx)
        self.trainx_unl = trainx.copy()
        self.testx = testx
        self.testy = testy.astype(np.int32)
        self.validx = trainx[-26032:].astype(th.config.floatX)  # validation set is now part of the training set
        self.validy = np.asarray(trainy[-26032:], dtype=np.int32)

        # select labeled data
        inds = data_rng.permutation(trainx.shape[0])
        trainx = trainx[inds]
        trainy = trainy[inds]
        self.trainx_lab = []
        self.trainy_lab = []
        for j in range(10):
            self.trainx_lab.append(trainx[trainy == j][:count])
            self.trainy_lab.append(trainy[trainy == j][:count])
        self.trainx_lab = np.concatenate(self.trainx_lab, axis=0)
        self.trainy_lab = np.concatenate(self.trainy_lab, axis=0)

        self.trainx_lab = self.trainx_lab[0:self.Nlabeled]
        self.trainx_unl = self.trainx_unl[0:self.Ni]

        print('Check shape of data')
        print('Shape of trainx_lab'),
        print(np.shape(self.trainx_lab))
        print('Shape of trainx_unl'),
        print(np.shape(self.trainx_unl))
        print('Shape of testx'),
        print(np.shape(self.testx))
        print('Shape of validx'),
        print(np.shape(self.validx))
        print('Shape of trainy_lab'),
        print(np.shape(self.trainy_lab))
        print('Shape of testy'),
        print(np.shape(self.testy))
        print('Shape of validy'),
        print(np.shape(self.validy))

        time.sleep(5)

    def load_reversible_cifar10(self):
        # This reversible dataset allow us to generate adversarial examples
        data_subfolder = os.path.join(self.data_dir, "cifar10")

        self.trainx_lab = np.load(os.path.join(data_subfolder, "X_train_processed.npy"))[0:self.Nlabeled, ]
        self.trainx_lab = self.trainx_lab.reshape([self.Nlabeled, self.Cin, self.H, self.W]).astype(th.config.floatX)
        self.trainx_unl = np.load(os.path.join(data_subfolder, "X_train_processed.npy"))
        self.trainx_unl = self.trainx_unl.reshape([self.trainx_unl.shape[0], self.Cin, self.H, self.W]).astype(
            th.config.floatX)

        self.testx = np.load(os.path.join(data_subfolder, "X_test_processed.npy"))
        self.testx = self.testx.reshape([self.testx.shape[0], self.Cin, self.H, self.W]).astype(th.config.floatX)
        self.trainy_lab = np.load(os.path.join(data_subfolder, "y_train_raw.npy"))[0:self.Nlabeled, ].astype(np.int32)
        self.testy = np.load(os.path.join(data_subfolder, "y_test_raw.npy")).astype(np.int32)

        self.validx = self.trainx_unl[40000:50000, :, :, :].astype(th.config.floatX)
        self.validy = np.load(os.path.join(data_subfolder, "y_train_raw.npy"))[40000:50000, ].astype(np.int32)
        self.print_data_dim()

    def load_reversible_blender_2axis(self):
        # This reversible_blender dataset allows us to generate adversarial examples
        data_subfolder = os.path.join(self.data_dir, "rendered_cifar10_2axis_full_rotation")

        self.trainx_lab = np.load(os.path.join(data_subfolder, "X_train_processed.npy"))[0:self.Nlabeled, ]
        self.trainx_lab = self.trainx_lab.reshape([self.Nlabeled, self.Cin, self.H, self.W]).astype(th.config.floatX)
        self.trainx_unl = np.load(os.path.join(data_subfolder, "X_train_processed.npy"))
        self.trainx_unl = self.trainx_unl.reshape([self.trainx_unl.shape[0], self.Cin, self.H, self.W]).astype(
            th.config.floatX)

        self.testx = np.load(os.path.join(data_subfolder, "X_test_processed.npy"))
        self.testx = self.testx.reshape([self.testx.shape[0], self.Cin, self.H, self.W]).astype(th.config.floatX)
        label_file = np.load(os.path.join(data_subfolder, "class_targets.npy"))
        self.trainy_lab = label_file[0:self.Nlabeled, ].astype(np.int32)
        self.testy = label_file[50000:60000, ].astype(np.int32)

        self.validx = self.trainx_unl[40000:50000, :, :, :].astype(th.config.floatX)
        self.validy = label_file[40000:50000, ].astype(np.int32)
        self.print_data_dim()

    def print_data_dim(self):
        # check dimension of data
        print "Shape of trainx_unl", self.trainx_unl.shape
        print "Shape of trainx_lab", self.trainx_lab.shape
        print "Shape of testx", self.testx.shape
        print "Shape of validx", self.validx.shape
        print "Shape of trainy_lab", self.trainy_lab.shape
        print "Shape of testy", self.testy.shape
        print "Shape of validy", self.validy.shape

