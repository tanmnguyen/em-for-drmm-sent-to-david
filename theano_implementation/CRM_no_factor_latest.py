__author__ = 'minhtannguyen'

#######################################################################################################################
# get rid of no rendering channel
# do both soft and hard on c
#######################################################################################################################


import matplotlib as mpl

mpl.use('Agg')

import numpy as np

np.set_printoptions(threshold='nan')

from theano.tensor.nnet import conv2d, sigmoid
from theano.tensor.signal import pool

# from old_codes.swmfa import *
import numpy
import theano
import theano.tensor as T
from theano.tensor.nlinalg import MatrixPinv

from nn_functions_latest import BatchNormalization
from theano.tensor.shared_randomstreams import RandomStreams


# from lasagne.layers import InputLayer, FeatureWTALayer

class CRM(object):
    """
    EM Algorithm for the Convolutional Mixture of Factor Analyzers.

    All of the equation numbers in this code follows those in "The EM Algorithm for Mixtures of Factor Analyzers" by
    Zoubin Ghahramani and Geoffrey E. Hinton

    calling arguments:

    [TBA]

    internal variables:
    `data_4D_lab`:  labeled images
    `data_4D_unl`:  unlabeld images
    `data_4D_unl_clean`: unlabeled images used for the clean pass without noise. This is used when we add noise to the
                            the model to make it robust. We are not doing so now.
    `noise_weight`: control how much noise we want to add to the model
    `noise_std`: the standard deviation of the noise
    `K`:           Number of components (lambdas: rendering matrices)
    `M`:           Latent dimensionality
    `W`:           Width of the input
    `H`:           Height of the input
    `w`:           Width of the rendering matrices
    `h`:           Height of the rendering matrices
    `Cin`:         Number of channels of the input = Number of channels of the rendering matrices
    `Ni`:          Number of input images
    `momentum_bn`: control how to update the batch mean and var in batch normalization which will be used in testing
    `is_train`:     ={0,1} where 0: testing mode and 1: training mode
    `lambdas_val_init`: Initial values for rendering matrices
    `amps_val_init`: Initial values for priors
    `pool_t_mode`: Pooling mode={'max_t','mean_t`,None}
    `border_mode`: {'valid`, 'full', `half`}
    `pool_a_mode`: {`relu`, None}
    `nonlin`: {`relu`, `abs`}
    `mean_pool_size`: size of the mean pooling layer before the softmax regression
    `max_condition_number`: a condition number to make computations more stable. Set to 1.e3
    `xavier_init`: {True, False} use Xavier initialization or not
    `is_noisy`: {True, False} add noise to the model or not
    `is_bn_BU`: {True, False} do batch normalization in the bottom-up E step or not
    `is_bn_TD`: {True, False} do batch normalization in the top-down E step or not
    `epsilon`: used to avoid divide by 0
    `momentum_pi_t`: used to update the pi_t during training (Exponential Moving Average (EMA) update)
    `momentum_pi_a`: used to update the pi_a during training (EMA update)
    `is_tied_bn`: {True, False} use the same batch mean and variance for batch normalization in Bottom-Up and Top-Down
                    E step
    `is_Dg`: {True, False} apply diagonal matrix multiplication instead of batch normalization (this is an alternative for batch
                normalization that we developed. (Still in development, a.k.a. it has not worked as we want yet)
    `is_prun`: {True, False} apply synaptic/neuron pruning or not (Stil in development)
    """

    def __init__(self, data_4D_lab,
                 data_4D_unl,
                 data_4D_unl_clean,
                 noise_weight,
                 noise_std,
                 K, M, W, H, w, h, Cin, Ni,
                 momentum_bn, is_train,
                 lambdas_val_init=None, amps_val_init=None,
                 pool_t_mode='max_t', border_mode='valid', pool_a_mode='relu', nonlin='relu',
                 mean_pool_size=(2, 2),
                 max_condition_number=1.e3,
                 xavier_init=False,
                 is_noisy=False,
                 is_bn_BU=False,
                 is_bn_TD=False,
                 epsilon=1e-10,
                 momentum_pi_t=0.99,
                 momentum_pi_a=0.99,
                 is_tied_bn=False,
                 is_Dg=False,
                 is_prun=False):

        # required
        self.K = K  # number of filters
        self.M = M  # latent dimensionality
        self.data_4D_lab = data_4D_lab # labeled data
        self.data_4D_unl = data_4D_unl # unlabeled data
        self.data_4D_unl_clean = data_4D_unl_clean # unlabeled data used for the clean path
        self.noise_weight = noise_weight
        self.noise_std = noise_std
        self.Ni = Ni  # no. of labeled examples = no. of unlabeled examples
        self.w = w  # width of filters
        self.h = h  # height of filters
        self.Cin = Cin  # number of channels in the image
        self.D = self.h * self.w * self.Cin  # patch size
        self.W = W  # width of image
        self.H = H  # height of image
        if border_mode == 'valid':
            self.Np = (self.H - self.h + 1) * (self.W - self.w + 1)  # no. of patches per image
            self.latents_shape = (self.Ni, self.K, self.H - self.h + 1, self.W - self.w + 1)
        elif border_mode == 'half':
            self.Np = self.H * self.W  # no. of patches per image
            self.latents_shape = (self.Ni, self.K, self.H, self.W)
        elif border_mode == 'full':
            self.Np = (self.H + self.h - 1) * (self.W + self.w - 1)  # no. of patches per image
            self.latents_shape = (self.Ni, self.K, self.H + self.h - 1, self.W + self.w - 1)
        else:
            print('Please specify self.Np and self.latents_shape in CRM_no_factor_latest.py')
            raise

        self.N = self.Ni * self.Np  # total no. of patches and total no. of hidden units
        self.mean_pool_size = mean_pool_size

        # self.means_val_init = means_val_init
        self.lambdas_val_init = lambdas_val_init
        self.amps_val_init = amps_val_init

        # options
        self.pool_t_mode = pool_t_mode
        self.pool_a_mode = pool_a_mode
        self.nonlin = nonlin
        self.border_mode = border_mode
        self.max_condition_number = max_condition_number
        self.xavier_init = xavier_init
        self.is_noisy = is_noisy
        self.is_bn_BU = is_bn_BU
        self.is_bn_TD = is_bn_TD
        self.momentum_bn = momentum_bn
        self.momentum_pi_t = momentum_pi_t
        self.momentum_pi_a = momentum_pi_a
        self.is_train = is_train
        self.epsilon = epsilon
        self.is_tied_bn = is_tied_bn
        self.is_Dg = is_Dg
        self.is_prun = is_prun

        self._initialize()

    def _initialize(self):
        #
        # initialize pi's, means, lambdas, psis, lambda_covs, covs, and inv_covs
        #

        # initialize the pi's (a.k.a the priors)
        # if initial values for pi's are not provided, randomly initialize pi's

        # set up a random number generator
        self.srng = RandomStreams()
        self.srng.seed(np.random.randint(2 ** 15))

        # add noise to the input if is_noisy
        if self.is_noisy:
            if self.data_4D_unl != None:
                self.data_4D_unl = self.data_4D_unl + \
                                   self.noise_weight * self.srng.normal(size=(self.Ni, self.Cin, self.H, self.W), avg=0.0, std=self.noise_std)
            if self.data_4D_lab != None:
                self.data_4D_lab = self.data_4D_lab + \
                                   self.noise_weight * self.srng.normal(size=(self.Ni, self.Cin, self.H, self.W),
                                                                        avg=0.0, std=self.noise_std)

        # initialize amps
        # amps is class prior  e.g. cat, dog probabilities
        if self.amps_val_init == None:
            amps_val = np.random.rand(self.K)
            amps_val /= np.sum(amps_val)

        else:
            amps_val = self.amps_val_init

        self.amps = theano.shared(value=np.asarray(amps_val, dtype=theano.config.floatX),
                                  name='amps', borrow=True)

        # initialize t and a priors
        self.pi_t = theano.shared(value=numpy.zeros(self.latents_shape[1:],
                                                     dtype=theano.config.floatX),name='pi_t', borrow=True)

        self.pi_a = theano.shared(value=numpy.zeros(self.latents_shape[1:],
                                                    dtype=theano.config.floatX), name='pi_a', borrow=True)

        if self.is_prun: # if do pruning, initialize the pruning matrix
            self.prun_mat = theano.shared(value=numpy.ones(self.latents_shape[1:],
                                          dtype=theano.config.floatX), name='prun_mat', borrow=True)

        # pi_t_final and pi_a_final are used after training for sampling
        self.pi_t_final = theano.shared(value=numpy.zeros(self.latents_shape[1:],
                                                    dtype=theano.config.floatX), name='pi_t_final', borrow=True)

        self.pi_a_final = theano.shared(value=numpy.zeros(self.latents_shape[1:],
                                                    dtype=theano.config.floatX), name='pi_a_final', borrow=True)

        # initialize the lambdas
        # if initial values for lambdas are not provided, randomly initialize lambdas
        if self.lambdas_val_init == None:
            if self.xavier_init:
                print('Do Xavier init')
                fan_in = self.D
                if self.pool_t_mode == None:
                    fan_out = self.K * self.h * self.w
                else:
                    fan_out = self.K * self.h * self.w / 4

                lambdas_bound = np.sqrt(6. / (fan_in + fan_out))
                lambdas_value = np.random.uniform(low=-lambdas_bound, high=lambdas_bound, size=(self.K, self.D, self.M))
            else:
                lambdas_value = np.random.randn(self.K, self.D, self.M) / \
                                np.sqrt(self.max_condition_number)
        else:
            lambdas_value = self.lambdas_val_init

        self.lambdas = theano.shared(value=np.asarray(lambdas_value, dtype=theano.config.floatX), name='lambdas',
                                     borrow=True)

        # Initialize BatchNorm
        if self.is_bn_BU:
            print('do batch_normalization in Bottom Up')
            self.bn_BU = BatchNormalization(insize=self.K, mode=1, momentum=self.momentum_bn, is_train=self.is_train,
                                            epsilon=self.epsilon)
            self.params = [self.lambdas, self.bn_BU.gamma, self.bn_BU.beta]
        else:
            self.params = [self.lambdas, ]

        # Matrix precondition.
        # TODO: still in development
        if self.is_Dg:
            print('use Dg')
            self.Dg = theano.shared(np.ones((self.K), dtype=theano.config.floatX), name='Dg', borrow=True)
            self.dg = theano.shared(np.zeros((self.K), dtype=theano.config.floatX), name='dg', borrow=True)
            self.params.append(self.Dg)
            self.params.append(self.dg)

        # Initialize the output
        self.output_lab = None
        self.output_clean = None
        self.output = None

    def take_EM_step(self):
        """
        Do one E step and then do one M step.
        """
        self._E_step_Bottom_Up()
        self._M_step()

    def get_important_latents_BU(self, input, betas):
        ##################################################################################
        # This function is used in the _E_step_Bottom_Up to compute latent representations
        ##################################################################################
        # compute E[z|x] using eq. 13
        latents_before_BN = conv2d(
            input=input,
            filters=betas,
            filter_shape=(self.K, self.Cin, self.h, self.w),
            image_shape=(self.Ni, self.Cin, self.H, self.W),
            filter_flip=False,
            border_mode=self.border_mode
        )

        # do Batch normalization
        if self.is_bn_BU:
            latents_after_BN = self.bn_BU.get_result(input=latents_before_BN, input_shape=self.latents_shape)
        elif self.is_Dg: # still in the beta state
            latents_after_BN = self.Dg.dimshuffle('x',0,'x','x') * (latents_before_BN - self.dg.dimshuffle('x',0,'x','x'))
        else:
            latents_after_BN = latents_before_BN

        if self.is_prun:
            latents = latents_after_BN * self.prun_mat
        else:
            latents = latents_after_BN

        # max over a
        if self.pool_a_mode == 'relu':
            print('Do max over a')
            max_over_a_mask = T.cast(T.gt(latents, 0.), theano.config.floatX)
        else:
            print('No max over a')
            max_over_a_mask = T.ones_like(latents)

        # max over t
        if self.pool_t_mode == 'max_t' and self.nonlin == 'relu':
            print('Do max over t')
            max_over_t_mask = T.grad(
                T.sum(pool.pool_2d(input=latents, ds=(2, 2), ignore_border=True, mode='max')),
                wrt=latents)  # argmax across t
            max_over_t_mask = T.cast(max_over_t_mask, theano.config.floatX)
        elif self.pool_t_mode == 'max_t' and self.nonlin == 'abs': # still in the beta state
            print('Do max over t')
            latents_abs = T.abs_(latents)
            max_over_t_mask = T.grad(
                T.sum(pool.pool_2d(input=latents_abs, ds=(2, 2), ignore_border=True, mode='max')),
                wrt=latents_abs)  # argmax across t
            max_over_t_mask = T.cast(max_over_t_mask, theano.config.floatX)
        else:
            print('No max over t')
            # compute latents masked by a
            max_over_t_mask = T.ones_like(latents)

        # compute latents masked by a and t
        if self.nonlin == 'relu':
            print('Nonlinearity is ReLU')
            latents_masked = latents * max_over_t_mask * max_over_a_mask  # * max_over_a_mask
        elif self.nonlin == 'abs':
            print('Nonlinearity is abs')
            latents_masked = T.abs_(latents) * max_over_t_mask
        else:
            print('Please specify your nonlin in CRM_no_factor_latest')
            raise

        masked_mat = max_over_t_mask * max_over_a_mask  # * max_over_a_mask

        if self.pool_t_mode == 'max_t':
            output = pool.pool_2d(input=latents_masked, ds=(2, 2),
                                  ignore_border=True, mode='average_exc_pad')
            output = output * 4.0
        elif self.pool_t_mode == 'mean_t':
            output = pool.pool_2d(input=latents_masked, ds=self.mean_pool_size,
                                  ignore_border=True, mode='average_exc_pad')
        else:
            output = latents_masked

        return latents_before_BN, latents, max_over_a_mask, max_over_t_mask, latents_masked, masked_mat, output


    def _E_step_Bottom_Up(self):
        """
        Expectation step through all clusters.
        Compute responsibilities, likelihoods, lambda dagger, latents, latent_covs, pi's
        """

        # Bottom-Up

        # compute the betas
        self.betas = self.lambdas.dimshuffle(0, 2, 1)

        betas = T.reshape(self.betas[:, 0, :], newshape=(self.K, self.Cin, self.h, self.w))

        # run bottom up for labeled examples.
        if self.is_bn_BU:
            if self.data_4D_lab != None and self.data_4D_unl_clean == None: # supervised training mode
                self.bn_BU.set_runmode(0)
            elif self.data_4D_lab != None and self.data_4D_unl_clean != None: # semisupervised training mode
                self.bn_BU.set_runmode(1)

        [self.latents_before_BN_lab, self.latents_lab, self.max_over_a_mask_lab,
         self.max_over_t_mask_lab, self.latents_masked_lab, self.masked_mat_lab, self.output_lab] \
            = self.get_important_latents_BU(input=self.data_4D_lab, betas=betas)

        # run bottom up for unlabeled data when noise is added
        if self.is_noisy:
            print('Noise is added to the model') # clean and noisy paths are different
            if self.data_4D_unl != None:
                if self.is_bn_BU:
                    self.bn_BU.set_runmode(1)  # no update of means and vars
                [self.latents_before_BN, self.latents, self.max_over_a_mask, self.max_over_t_mask, self.latents_masked, self.masked_mat,
                 self.output] \
                    = self.get_important_latents_BU(input=self.data_4D_unl, betas=betas)

            if self.data_4D_unl_clean != None:
                if self.is_bn_BU:
                    self.bn_BU.set_runmode(0)  # using clean data to keep track the means and the vars
                [self.latents_before_BN_clean, self.latents_clean, self.max_over_a_mask_clean, self.max_over_t_mask_clean, self.latents_masked_clean,
                 self.masked_mat_clean, self.output_clean] \
                    = self.get_important_latents_BU(input=self.data_4D_unl_clean, betas=betas)

        else:
            print('No noise is added') # clean and noisy paths are the same
            if self.data_4D_unl != None:
                if self.is_bn_BU:
                    self.bn_BU.set_runmode(0) # using clean data to keep track the means and vars
                [self.latents_before_BN, self.latents, self.max_over_a_mask, self.max_over_t_mask, self.latents_masked, self.masked_mat,
                 self.output] \
                    = self.get_important_latents_BU(input=self.data_4D_unl, betas=betas)
                self.output_clean = self.output
                # compute the posterior for t

        if self.data_4D_unl != None: # if supervised learning
            self.pi_t_minibatch = T.mean(self.max_over_t_mask, axis=0)
            self.pi_a_minibatch = T.mean(self.max_over_a_mask, axis=0)
            self.pi_t_new = self.momentum_pi_t*self.pi_t + (1 - self.momentum_pi_t)*self.pi_t_minibatch
            self.pi_a_new = self.momentum_pi_a*self.pi_a + (1 - self.momentum_pi_a)*self.pi_a_minibatch

            self.amps_new = T.sum(self.masked_mat, axis=(0, 2, 3)) / float(self.N / 4)
            self.rs = self.masked_mat
            self.logLs = 0.5 * T.sum(self.masked_mat, axis=(1,))

        else:
            self.pi_t_minibatch = T.mean(self.max_over_t_mask_lab, axis=0)
            self.pi_a_minibatch = T.mean(self.max_over_a_mask_lab, axis=0)
            self.pi_t_new = self.momentum_pi_t * self.pi_t + (1 - self.momentum_pi_t) * self.pi_t_minibatch
            self.pi_a_new = self.momentum_pi_a * self.pi_a + (1 - self.momentum_pi_a) * self.pi_a_minibatch

            self.amps_new = T.sum(self.masked_mat_lab, axis=(0, 2, 3)) / float(self.N / 4)
            self.rs = self.masked_mat_lab
            self.logLs = 0.5 * T.sum(self.masked_mat_lab, axis=(1,))

        # if self.pool_t_mode == 'max_t':
        #     exp_latents = T.exp(self.latents)
        #     sum_exp_latents = pool.pool_2d(input=exp_latents, ds=(2, 2),
        #                                    ignore_border=True, mode='average_exc_pad')
        #     sum_exp_latents = sum_exp_latents * 4.0
        #     posterior_t = exp_latents / sum_exp_latents.repeat(2, axis=2).repeat(2, axis=3)
        #     prior_t = T.mean(posterior_t, axis=0)
        #     self.KLD_t = T.mean(posterior_t * T.log(posterior_t/prior_t.dimshuffle('x', 0, 1, 2) + 1e-8)) * 4.0

            # IF DO M STEP, REMEMBER TO RESHAPE SELF.LATENTS TO (K, M, N)

    def _E_step_Top_Down_Reconstruction(self, mu_cg, denoising='simple', top_down_mode=None, is_TD_relu=False):
        # Reconstruct/Sample the images to compute the complete-data log-likelihood
        # input is of size Ni x K x (H-h+1)/2 x (W-w+1)/2
        #

        # Up-sample the latent presentations
        if self.pool_t_mode == 'max_t':
            print('Pool_Mode is %s' % self.pool_t_mode)
            self.latents_unpooled_before_BN = mu_cg.repeat(2, axis=2).repeat(2, axis=3)
        elif self.pool_t_mode == 'mean_t':
            print('Pool_Mode is %s' % self.pool_t_mode)
            self.latents_unpooled_before_BN = mu_cg.repeat(self.mean_pool_size[0], axis=2).repeat(self.mean_pool_size[1], axis=3)
        elif self.pool_t_mode == None:
            print('Pool_Mode is None')
            self.latents_unpooled_before_BN = mu_cg
        else:
            print('Please specify your TopDown mode in CRM_no_factor_latest.py')
            raise

        if self.is_bn_TD: # if doing batch normalization in the Top-Down
            print('do batch_normalization in TopDown')
            if not self.is_tied_bn: # if Top-Down Batch Norm is tied to Bottom-Up Batch Norm
                self.bn_TD = BatchNormalization(insize=self.K, mode=1, momentum=self.momentum_bn,
                                                is_train=self.is_train,
                                                epsilon=self.epsilon)
                self.bn_TD.set_runmode(1)
                self.params.append(self.bn_TD.gamma)
                self.params.append(self.bn_TD.beta)
                self.latents_unpooled_after_BN = self.bn_TD.get_result(input=self.latents_unpooled_before_BN,
                                                                       input_shape=self.latents_shape)
            else:
                print('Tie BN_BU and BN_TD')
                self.bn_BU.set_runmode(1)
                self.latents_unpooled_after_BN = self.bn_BU.get_result(input=self.latents_unpooled_before_BN, input_shape=self.latents_shape)
        elif self.is_Dg: # If using matrix preconditioning instead (still in development)
            self.latents_unpooled_after_BN = T.inv(self.Dg).dimshuffle('x',0,'x','x') * self.latents_unpooled_before_BN + self.dg.dimshuffle('x',0,'x','x')
        else:
            self.latents_unpooled_after_BN = self.latents_unpooled_before_BN

        if denoising == 'simple': # if no denoising
            print('Do not use any combinator to denoise')
            self.latents_final = self.latents_unpooled_after_BN
        elif denoising == 'message-passing': # if doing denoising using message-passing approach
            print('Use message-passing approach to update the latents')
            latents_final = self.latents_unpooled_after_BN * self.latents
            self.bn_mp = BatchNormalization(insize=self.K, mode=1, momentum=self.momentum_bn,
                                            is_train=self.is_train,
                                            epsilon=self.epsilon)
            self.bn_mp.set_runmode(1)
            self.params.append(self.bn_mp.gamma)
            self.params.append(self.bn_mp.beta)
            self.latents_final = self.bn_mp.get_result(input=latents_final, input_shape=self.latents_shape)
        else:
            print('Please implement your own mode in the _Top_Down function in CRM_no_factor_latest.py')
            raise

        if is_TD_relu: # if applying ReLU in Top-Down
            print('Apply ReLU on the reconstructed images')
            self.latents_unpooled_no_mask = T.nnet.relu(self.latents_final)
        else:
            self.latents_unpooled_no_mask = self.latents_final

        if top_down_mode == 'signed': # if using signed DRMM (still in development)
            if denoising == 'simple': # if no denoising
                print('Do signed TopDown')
                self.s_TD = T.sgn(self.latents_final)
                self.s_TD = T.cast(self.s_TD, theano.config.floatX)
                self.signed_latents = self.latents * self.s_TD

                if self.pool_t_mode == 'max_t':
                    self.max_over_t_mask_TD = T.grad(
                        T.sum(pool.pool_2d(input=self.signed_latents, ds=(2, 2), ignore_border=True, mode='max')),
                        wrt=self.signed_latents)  # argmax across t
                    self.max_over_t_mask_TD = T.cast(self.max_over_t_mask_TD, theano.config.floatX)
                else:
                    self.max_over_t_mask_TD = T.ones_like(self.latents)

                self.max_over_a_mask_TD = T.cast(T.gt(self.signed_latents * self.max_over_t_mask_TD, 0.),
                                                 theano.config.floatX)
                self.max_over_a_mask_TD = T.cast(self.max_over_a_mask_TD, theano.config.floatX)
                self.final_mask_mat = self.max_over_t_mask_TD * self.max_over_a_mask_TD
                self.latents_unpooled = self.latents_unpooled_no_mask * self.final_mask_mat

            elif denoising == 'message-passing': # if doing denoising using message-passing approach
                print('Do signed TopDown')
                if self.pool_t_mode == 'max_t':
                    self.max_over_t_mask_TD = T.grad(
                        T.sum(pool.pool_2d(input=self.latents_final, ds=(2, 2), ignore_border=True, mode='max')),
                        wrt=self.latents_final)  # argmax across t
                    self.max_over_t_mask_TD = T.cast(self.max_over_t_mask_TD, theano.config.floatX)
                else:
                    self.max_over_t_mask_TD = T.ones_like(self.latents_final)

                self.max_over_a_mask_TD = T.cast(T.gt(self.latents_final * self.max_over_t_mask_TD, 0.),
                                                 theano.config.floatX)
                self.max_over_a_mask_TD = T.cast(self.max_over_a_mask_TD, theano.config.floatX)
                self.final_mask_mat = self.max_over_t_mask_TD * self.max_over_a_mask_TD
                self.latents_unpooled = self.latents_unpooled_no_mask * self.final_mask_mat

        elif top_down_mode == 'normal': # if using nonnegative DRMM
            print('Only use BottomUp nuisances')
            self.final_mask_mat = self.masked_mat
            if self.is_prun:
                self.latents_unpooled = self.latents_unpooled_no_mask * self.final_mask_mat * self.prun_mat
            else:
                self.latents_unpooled = self.latents_unpooled_no_mask * self.final_mask_mat

        else:
            print('Please specify your top_down_mode in CRM_no_factor')
            raise

        # reconstruct/sample the image
        self.lambdas_deconv = (T.reshape(self.lambdas[:, :, 0],
                                    newshape=(self.K, self.Cin, self.h, self.w))).dimshuffle(1, 0, 2, 3)
        self.lambdas_deconv = self.lambdas_deconv[:, :, ::-1, ::-1]

        if self.border_mode == 'valid':
            self.data_reconstructed = conv2d(
                input=self.latents_unpooled,
                filters=self.lambdas_deconv,
                filter_shape=(self.Cin, self.K, self.h, self.w),
                image_shape=(self.Ni, self.K, self.H - self.h + 1, self.W - self.w + 1),
                filter_flip=False,
                border_mode='full'
            )
        elif self.border_mode == 'half':
            self.data_reconstructed = conv2d(
                input=self.latents_unpooled,
                filters=self.lambdas_deconv,
                filter_shape=(self.Cin, self.K, self.h, self.w),
                image_shape=(self.Ni, self.K, self.H, self.W),
                filter_flip=False,
                border_mode='half'
            )
        else:
            self.data_reconstructed = conv2d(
                input=self.latents_unpooled,
                filters=self.lambdas_deconv,
                filter_shape=(self.Cin, self.K, self.h, self.w),
                image_shape=(self.Ni, self.K, self.H + self.h - 1, self.W + self.w - 1),
                filter_flip=False,
                border_mode='valid'
            )

        # compute reconstruction error
        self.reconstruction_error = T.mean((self.data_4D_unl_clean - self.data_reconstructed) ** 2)

    def _E_step_Top_Down_Sampling(self, mu_cg, denoising='simple', top_down_mode=None, is_TD_relu=False,
                                  num_TD_samplings=1):
        #
        # This function is used to sample images from DRMM after training
        # input is of size Ni x K x (H-h+1)/2 x (W-w+1)/2
        #
        if self.pool_t_mode == 'max_t':
            print('Pool_Mode is %s' % self.pool_t_mode)
            latents_unpooled_before_BN = mu_cg.repeat(2, axis=2).repeat(2, axis=3)
        elif self.pool_t_mode == 'mean_t':
            print('Pool_Mode is %s' % self.pool_t_mode)
            latents_unpooled_before_BN = mu_cg.repeat(self.mean_pool_size[0], axis=2).repeat(self.mean_pool_size[1],
                                                                                             axis=3)
        elif self.pool_t_mode == None:
            print('Pool_Mode is None')
            latents_unpooled_before_BN = mu_cg
        else:
            print('Please specify your TopDown mode in CRM_no_factor_latest.py')
            raise

        if self.is_bn_TD:
            print('do batch_normalization in TopDown')
            if not self.is_tied_bn:
                bn_TD = BatchNormalization(insize=self.K, mode=1, momentum=self.momentum_bn,
                                           is_train=self.is_train,
                                           epsilon=self.epsilon)
                bn_TD.set_runmode(1)
                self.params.append(bn_TD.gamma)
                self.params.append(bn_TD.beta)
                latents_unpooled_after_BN = bn_TD.get_result(input=latents_unpooled_before_BN,
                                                             input_shape=self.latents_shape)
            else:
                print('Tie BN_BU and BN_TD')
                self.bn_BU.set_runmode(1)
                latents_unpooled_after_BN = self.bn_BU.get_result(input=latents_unpooled_before_BN,
                                                                  input_shape=self.latents_shape)
        elif self.is_Dg:
            latents_unpooled_after_BN = T.inv(self.Dg).dimshuffle('x', 0, 'x',
                                                                  'x') * latents_unpooled_before_BN + self.dg.dimshuffle(
                'x', 0, 'x', 'x')
        else:
            latents_unpooled_after_BN = latents_unpooled_before_BN

        if denoising == 'simple':
            print('Do not use any combinator to denoise')
            latents_final = latents_unpooled_after_BN
        elif denoising == 'message-passing':
            print('Use message-passing approach to update the latents')
            latents_final = latents_unpooled_after_BN * self.latents
            bn_mp = BatchNormalization(insize=self.K, mode=1, momentum=self.momentum_bn,
                                       is_train=self.is_train,
                                       epsilon=self.epsilon)
            bn_mp.set_runmode(1)
            self.params.append(bn_mp.gamma)
            self.params.append(bn_mp.beta)
            latents_final = bn_mp.get_result(input=latents_final, input_shape=self.latents_shape)
        else:
            print('Please implement your own mode in the _Top_Down function in CRM_no_factor_latest.py')
            raise

        if is_TD_relu:
            print('Apply ReLU on the reconstructed images')
            latents_unpooled_no_mask = T.nnet.relu(latents_final)
        else:
            latents_unpooled_no_mask = latents_final

        if top_down_mode == 'signed':
            if denoising == 'simple':
                print('Do signed TopDown')
                s_TD = T.sgn(latents_final)
                s_TD = T.cast(s_TD, theano.config.floatX)
                signed_latents = self.latents * s_TD

                if self.pool_t_mode == 'max_t':
                    max_over_t_mask_TD = T.grad(
                        T.sum(pool.pool_2d(input=signed_latents, ds=(2, 2), ignore_border=True, mode='max')),
                        wrt=signed_latents)  # argmax across t
                    max_over_t_mask_TD = T.cast(max_over_t_mask_TD, theano.config.floatX)
                else:
                    max_over_t_mask_TD = T.ones_like(self.latents)

                max_over_a_mask_TD = T.cast(T.gt(signed_latents * max_over_t_mask_TD, 0.),
                                            theano.config.floatX)
                max_over_a_mask_TD = T.cast(max_over_a_mask_TD, theano.config.floatX)
                final_mask_mat = max_over_t_mask_TD * max_over_a_mask_TD
                latents_unpooled = latents_unpooled_no_mask * final_mask_mat

            elif denoising == 'message-passing':
                print('Do signed TopDown')
                if self.pool_t_mode == 'max_t':
                    max_over_t_mask_TD = T.grad(
                        T.sum(pool.pool_2d(input=latents_final, ds=(2, 2), ignore_border=True, mode='max')),
                        wrt=latents_final)  # argmax across t
                    max_over_t_mask_TD = T.cast(max_over_t_mask_TD, theano.config.floatX)
                else:
                    max_over_t_mask_TD = T.ones_like(latents_final)

                max_over_a_mask_TD = T.cast(T.gt(latents_final * max_over_t_mask_TD, 0.),
                                            theano.config.floatX)
                max_over_a_mask_TD = T.cast(max_over_a_mask_TD, theano.config.floatX)
                final_mask_mat = max_over_t_mask_TD * max_over_a_mask_TD
                latents_unpooled = latents_unpooled_no_mask * final_mask_mat

        elif top_down_mode == 'normal':
            print('Only use BottomUp nuisances')
            final_mask_mat = T.tile(self.masked_mat, (num_TD_samplings, 1, 1, 1))
            latents_unpooled = latents_unpooled_no_mask * final_mask_mat
        else:
            print('Please specify your top_down_mode in CRM_no_factor')
            raise

        lambdas_deconv = (T.reshape(self.lambdas[:, :, 0],
                                    newshape=(self.K, self.Cin, self.h, self.w))).dimshuffle(1, 0, 2, 3)
        lambdas_deconv = lambdas_deconv[:, :, ::-1, ::-1]

        if self.border_mode == 'valid':
            data_reconstructed = conv2d(
                input=latents_unpooled,
                filters=lambdas_deconv,
                filter_shape=(self.Cin, self.K, self.h, self.w),
                image_shape=(num_TD_samplings * self.Ni, self.K, self.H - self.h + 1, self.W - self.w + 1),
                filter_flip=False,
                border_mode='full'
            )
        elif self.border_mode == 'half':
            data_reconstructed = conv2d(
                input=latents_unpooled,
                filters=lambdas_deconv,
                filter_shape=(self.Cin, self.K, self.h, self.w),
                image_shape=(num_TD_samplings * self.Ni, self.K, self.H, self.W),
                filter_flip=False,
                border_mode='half'
            )
        else:
            data_reconstructed = conv2d(
                input=latents_unpooled,
                filters=lambdas_deconv,
                filter_shape=(self.Cin, self.K, self.h, self.w),
                image_shape=(num_TD_samplings * self.Ni, self.K, self.H + self.h - 1, self.W + self.w - 1),
                filter_flip=False,
                border_mode='valid'
            )

        self.sampled_I_using_BU_g = data_reconstructed
        self.sampled_z_using_BU_g = latents_unpooled_after_BN

    def _M_step(self):
        """
        Still in development

        Maximization step through all clusters

        Update parameters to optimize the log-likelihood

        This assumes that `_E_step()` has been run.
        """

        # make latents_tilde (according to the equations at the top of page 7 in Hinton's paper)
        latents_tilde = T.concatenate([self.latents,
                                       theano.shared(value=np.ones((self.K, 1, self.N),
                                                                   dtype=theano.config.floatX), borrow=True)],
                                      axis=1)

        # make latents_covs_tilde (according to the equations at the top of page 7 in Hinton's paper)
        col1 = T.concatenate([self.latent_covs, (self.latents[:, :, None, :]).dimshuffle((0, 2, 1, 3))], axis=1)
        latent_covs_tilde = T.concatenate([col1, latents_tilde[:, :, None, :]], axis=2)

        # compute lambdas_tilde (equation 15 in Hinton's paper)
        lambdas_tilde, updates22 = theano.scan(
            fn=lambda lt, resp, lt_covs, z: T.dot(T.dot(z[:, None, :] * lt[None, :, :], resp),
                                                  MatrixPinv()(T.dot(lt_covs, resp))),
            outputs_info=None,
            sequences=[latents_tilde[self.indx_good_cluster], self.rs[self.indx_good_cluster],
                       latent_covs_tilde[self.indx_good_cluster]],
            non_sequences=self.data.T)

        # update lambdas and means
        lambdas_new = lambdas_tilde[:, :, 0:self.M]
        self.lambdas_new = T.set_subtensor(self.lambdas[self.indx_good_cluster], lambdas_new)
        means_new = lambdas_tilde[:, :, self.M]
        self.means_new = T.set_subtensor(self.means[self.indx_good_cluster], means_new)

        # update covs
        self._update_covs()

    def _update_covs(self):
        """
        Update covs and inv_covs
        """

        self.covs_new, updates1 = theano.scan(fn=lambda l: T.dot(l, T.transpose(l)),
                                              outputs_info=None,
                                              sequences=[self.lambdas_new])

        self.inv_covs_new, updates2 = theano.scan(
            fn=lambda l: T.dot(l, T.transpose(l)) / T.sqr(T.sum(T.sqr(l))),
            outputs_info=None,
            sequences=self.lambdas_new)