__author__ = 'tannguyen'

import matplotlib as mpl
mpl.use('Agg')
from pylab import *

from matplotlib import rcParams
rcParams.update({'figure.autolayout': True})

import os

from PIL import Image

import numpy as np

from load_data_latest import DATA

import theano
import theano.tensor as T

from theano.misc.pkl_utils import load
from theano.tensor.shared_randomstreams import RandomStreams
from theano.tensor.signal import pool

from theano.tensor.nnet import conv2d


#from guppy import hpy; h=hpy()

class Probe(object):
    '''
    This probe class is used to analyze a trained model
    '''

    def __init__(self, model, output_dir):
        self.model = model
        self.output_dir = output_dir

        if not os.path.exists(self.output_dir):
            os.makedirs(self.output_dir)

    def compute_NN_violations(self, input, which_layer):
        getzTopDown = theano.function([self.model.x_unl, self.model.is_train, self.model.momentum_bn],
                                      self.model.layers[which_layer].latents_unpooled_after_BN, on_unused_input='warn')
        zTopDown = getzTopDown(input, 0, 1.0)
        num_nn_violation = (zTopDown < 0).sum()
        num_nn_sof_violation = (zTopDown < -0.01).sum()
        max_abs_z = np.max(np.abs(zTopDown))
        mean_abs_z = np.mean(np.abs(zTopDown))
        return num_nn_violation, num_nn_sof_violation, np.size(zTopDown), max_abs_z, mean_abs_z

    def reconstruct_images(self, input, Nimages):
        # Reconstruct images
        getReconstruction = theano.function([self.model.x_unl, self.model.is_train, self.model.momentum_bn], self.model.layers[self.model.N_layer - 1].data_reconstructed, on_unused_input='warn')
        I_hat = getReconstruction(input, 0, 1.0)

        I_hat = I_hat.transpose((0, 2, 3, 1))
        I_hat = I_hat[0:Nimages]
        return I_hat

    def sampling_from_layer(self, which_layer, input, mask_mat):
        if self.model.layers[which_layer].pool_t_mode == 'max_t':
            print('Pool_Mode is %s' % self.model.layers[which_layer])
            latents_unpooled_before_BN = input.repeat(2, axis=2).repeat(2, axis=3)
        elif self.model.layers[which_layer].pool_t_mode == 'mean_t':
            print('Pool_Mode is %s' % self.model.layers[which_layer].pool_t_mode)
            latents_unpooled_before_BN = input.repeat(self.model.layers[which_layer].mean_pool_size[0],
                                                      axis=2).repeat(self.model.layers[which_layer].mean_pool_size[1], axis=3)
        elif self.model.layers[which_layer].pool_t_mode == None:
            print('Pool_Mode is None')
            latents_unpooled_before_BN = input
        else:
            print('Please specify your TopDown mode in CRM_no_factor_latest.py')
            raise

        latents_unpooled_after_BN = latents_unpooled_before_BN

        latents_final = latents_unpooled_after_BN

        latents_unpooled_no_mask = latents_final

        final_mask_mat = mask_mat
        latents_unpooled = latents_unpooled_no_mask * final_mask_mat

        lambdas_deconv = (T.reshape(self.model.layers[which_layer].lambdas[:, :, 0],
                                    newshape=(self.model.layers[which_layer].K, self.model.layers[which_layer].Cin,
                                              self.model.layers[which_layer].h,
                                              self.model.layers[which_layer].w))).dimshuffle(1, 0, 2, 3)
        lambdas_deconv = lambdas_deconv[:, :, ::-1, ::-1]

        if self.model.layers[which_layer].border_mode == 'valid':
            data_reconstructed = conv2d(
                input=latents_unpooled,
                filters=lambdas_deconv,
                filter_shape=(self.model.layers[which_layer].Cin, self.model.layers[which_layer].K,
                              self.model.layers[which_layer].h, self.model.layers[which_layer].w),
                image_shape=(self.model.layers[which_layer].Ni, self.model.layers[which_layer].K,
                             self.model.layers[which_layer].H - self.model.layers[which_layer].h + 1,
                             self.model.layers[which_layer].W - self.model.layers[which_layer].w + 1),
                filter_flip=False,
                border_mode='full'
            )
        elif self.model.layers[which_layer].border_mode == 'half':
            data_reconstructed = conv2d(
                input=latents_unpooled,
                filters=lambdas_deconv,
                filter_shape=(self.model.layers[which_layer].Cin, self.model.layers[which_layer].K,
                              self.model.layers[which_layer].h, self.model.layers[which_layer].w),
                image_shape=(self.model.layers[which_layer].Ni, self.model.layers[which_layer].K,
                             self.model.layers[which_layer].H, self.model.layers[which_layer].W),
                filter_flip=False,
                border_mode='half'
            )
        else:
            data_reconstructed = conv2d(
                input=latents_unpooled,
                filters=lambdas_deconv,
                filter_shape=(self.model.layers[which_layer].Cin, self.model.layers[which_layer].K,
                              self.model.layers[which_layer].h, self.model.layers[which_layer].w),
                image_shape=(self.model.layers[which_layer].Ni, self.model.layers[which_layer].K,
                             self.model.layers[which_layer].H + self.model.layers[which_layer].h - 1,
                             self.model.layers[which_layer].W + self.model.layers[which_layer].w - 1),
                filter_flip=False,
                border_mode='valid'
            )

        return data_reconstructed

    def sample_images(self, mode, Nimages=1):
        srng = RandomStreams()
        srng.seed(np.random.randint(2 ** 15))

        class_index = srng.random_integers(size=(Nimages,), low=0, high=self.model.K_Softmax-1, ndim=None,
                        dtype='int32')
        class_one_hot_encoding = T.extra_ops.to_one_hot(T.ones_like(class_index) * 6, 10)
        top_input = T.dot(class_one_hot_encoding, self.model.RegressionInSoftmax.W.T)
        top_input = top_input.dimshuffle(0, 1, 'x', 'x')

        uniform_tensor = srng.uniform(size=self.model.layers[0].latents_shape, low=0, high=1.0,
                                      ndim=None, dtype=theano.config.floatX)
        if mode == 'uniform':
            mask_gen_a = T.cast(T.gt(uniform_tensor, 0.5), theano.config.floatX)
            uniform_tensor_new = uniform_tensor
        elif mode == 'proper_using_pi':
            mask_gen_a = T.cast(T.gt(uniform_tensor, 1.0 - self.model.layers[0].pi_a),
                                                     theano.config.floatX)
            uniform_tensor_new = uniform_tensor * self.model.layers[0].pi_t
        elif mode == 'proper_using_final_pi':
            mask_gen_a = T.cast(T.gt(uniform_tensor, 1.0 - self.model.layers[0].pi_a_final),
                                                     theano.config.floatX)
            uniform_tensor_new = uniform_tensor * self.model.layers[0].pi_t_final

        if self.model.layers[0].pool_t_mode == 'max_t':
            mask_gen_t = T.grad(T.sum(T.max(pool.pool_2d(input=uniform_tensor_new, ds=(2, 2),
                                                                              ignore_border=True, mode='max'), axis=1)),
                                                     wrt=uniform_tensor_new)
            mask_gen = mask_gen_t * mask_gen_a
        else:
            mask_gen = mask_gen_a

        self.model.layers[0].samples = self.sampling_from_layer(which_layer=0, input=top_input, mask_mat=mask_gen)

        for i in xrange(1, self.model.N_layer):
            uniform_tensor = srng.uniform(size=self.model.layers[i].latents_shape, low=0, high=1.0,
                                          ndim=None, dtype=theano.config.floatX)
            if mode == 'uniform':
                mask_gen_a = T.cast(T.gt(uniform_tensor, 0.5), theano.config.floatX)
                uniform_tensor_new = uniform_tensor
            elif mode == 'proper_using_pi':
                mask_gen_a = T.cast(T.gt(uniform_tensor, 1.0 - self.model.layers[i].pi_a), theano.config.floatX)
                uniform_tensor_new = uniform_tensor * self.model.layers[i].pi_t
            elif mode == 'proper_using_final_pi':
                mask_gen_a = T.cast(T.gt(uniform_tensor, 1.0 - self.model.layers[i].pi_a_final), theano.config.floatX)
                uniform_tensor_new = uniform_tensor * self.model.layers[i].pi_t_final
            else:
                print('Use t and a from the bottom up to sample')


            if self.model.layers[i].pool_t_mode == 'max_t':
                mask_gen_t = T.grad(T.sum(T.max(pool.pool_2d(input=uniform_tensor_new, ds=(2, 2),
                                                                                  ignore_border=True, mode='max'), axis=1)),
                                                         wrt=uniform_tensor_new)
                mask_gen = mask_gen_t * mask_gen_a
            else:
                mask_gen = mask_gen_a

            self.model.layers[i].samples = self.sampling_from_layer(which_layer=i, input=self.model.layers[i-1].samples, mask_mat=mask_gen)

        getSamples = theano.function([], self.model.layers[self.model.N_layer-1].samples,
                                     on_unused_input='warn')
        I_hat = getSamples()

        I_hat = I_hat.transpose((0, 2, 3, 1))
        I_hat = I_hat[0:Nimages]
        return I_hat

    def compute_softmax_related_vars(self, input, label):
        # Reconstruct images
        get_log_posteriors = theano.function([self.model.x, self.model.y, self.model.is_train, self.model.momentum_bn],
                                             [self.model.softmax_layer_nonlin.gammas_semisupervised,
                                              self.model.supervisedNLL, self.model.softmax_layer_nonlin.y_pred,
                                              self.model.y],
                                             updates=[], on_unused_input='ignore')

        [p, ce, ypred, ytruth] = get_log_posteriors(input, label, 0, 1.0)

        return None

def plot_image_table(imgs, output_dir, file_name):
    N = imgs.shape[0]
    for i in xrange(N):
        if imgs.shape[3] == 1:
            imshow(imgs[i, :, :, 0], cmap='gray')
        else:
            imshow(imgs[i])
        axis('off')
        out_path = os.path.join(output_dir, 'single_images')
        if not os.path.exists(out_path):
            os.makedirs(out_path)

        out_file = os.path.join(out_path, '%s_%i.jpg' % (file_name,i))
        savefig(out_file)
        close()
    sub_x_size = int(sqrt(N))
    sub_y_size = int(sqrt(N))
    img = Image.new('RGB', (800 * sub_x_size, 600 * sub_y_size))
    for i in xrange(N):
        this_img = Image.open(os.path.join(out_path, '%s_%i.jpg' % (file_name,i)))
        x_loc = (i % sub_x_size) * 800
        y_loc = (i / sub_y_size) * 600
        img.paste(this_img, (x_loc, y_loc))
    fig = imshow(img)
    axis('off')
    fig.axes.get_xaxis().set_visible(False)
    fig.axes.get_yaxis().set_visible(False)
    savefig(os.path.join(output_dir, '%ss.pdf'%file_name))
    savefig(os.path.join(output_dir, '%ss.jpg' %file_name))
    close()

def runProbe(model_file_name, training_name, data_mode='MNIST'):
    #
    # Run the model analysis
    #
    Nimages = 50

    if data_mode == 'MNIST':
        N = 50
        data_dir = '/home/ubuntu/Tan-repos/em-for-drm-repeat-labels/data/mnist.npz'
        mnist = DATA(dataset_name='MNIST', data_mode='all', data_dir=data_dir, Nlabeled=50000, Ni=50000, Cin=1, H=28, W=28, seed=5)
        dat = mnist.testx
    elif data_mode == 'CIFAR10':
        N = 50
        data_dir = '/home/ubuntu/repos/em-for-drm-repeat-labels/data/cifar10'
        cifar10 = DATA(dataset_name='CIFAR10', data_mode='all', data_dir=data_dir, Nlabeled=50000, Ni=50000, Cin=3, H=32, W=32, seed=5, preprocess=True)
        dat = cifar10.testx
    else:
        print('Please specify your data_mode in analyze_DRM_latest')
        raise

    N_batch = np.shape(dat)[0] / N
    root_dir = '/home/ubuntu/research_results/EM_results/results-for-analysis'
    model_dir = os.path.join(root_dir, training_name, 'Train/params', model_file_name)
    output_dir = os.path.join(root_dir, training_name, 'Train', 'model_analysis_results')
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    output_dir = os.path.join(output_dir,model_file_name[:-4])
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    with open(model_dir, 'rb') as f:
        model = load(f)

    probe = Probe(model=model, output_dir=output_dir)

    I = dat[0:Nimages]
    I = I.transpose((0, 2, 3, 1))
    print('Shape of I original')
    print(np.shape(I))

    # total_nn_violation = 0
    # total_nn_soft_violation = 0
    # total_z = 0
    # total_max_abs_z = 0
    # total_mean_abs_z = 0
    # for i in xrange(model.N_layer):
    #     num_nn_violation = 0
    #     num_nn_soft_violation = 0
    #     num_z = 0
    #     max_abs_z = 0
    #     mean_abs_z = 0
    #     for minibatch_index in xrange(20):
    #         nvio, nsoft, lsize, maxaz, meanaz = probe.compute_NN_violations(input=dat[minibatch_index * N:(minibatch_index + 1) * N], which_layer=i)
    #         num_nn_violation += nvio
    #         num_nn_soft_violation += nsoft
    #         num_z += lsize
    #         max_abs_z = np.max([max_abs_z, maxaz])
    #         mean_abs_z += meanaz
    #
    #     print('(%i, %i, %i)'%(model.layers[i].Cin, model.layers[i].W, model.layers[i].H))
    #     print('Percent_NN_Violation at layer %i: %f'%(model.N_layer-i, num_nn_violation/float(num_z)))
    #     print('Percent_NN_Soft_Violation (z < -0.01) at layer %i: %f' % (model.N_layer - i, num_nn_soft_violation / float(num_z)))
    #     print('Max_abs_z: %f'%max_abs_z)
    #     print('Mean_abs_z: %f' %(mean_abs_z/20))
    #
    #     total_nn_violation += num_nn_violation
    #     total_nn_soft_violation += num_nn_soft_violation
    #     total_z += num_z
    #     total_max_abs_z = np.max([total_max_abs_z, max_abs_z])
    #     total_mean_abs_z += mean_abs_z/20
    #
    # print('')
    # print('Total percent_NN_Violation: %f'%(total_nn_violation/float(total_z)))
    # print('Total percent_NN_Soft_Violation (z < -0.01): %f' % (total_nn_soft_violation / float(total_z)))
    # print('Total max_abs_z: %f'%total_max_abs_z)
    # print('Total mean_abs_z: %f' %(total_mean_abs_z/model.N_layer))

    plot_image_table(imgs=I, output_dir=output_dir, file_name='OriginalImage')

    I_reconstructed = probe.reconstruct_images(input=dat, Nimages=Nimages)
    print('Shape of I reconstructed')
    print(np.shape(I_reconstructed))
    plot_image_table(imgs=I_reconstructed, output_dir=output_dir, file_name='ReconstructedImage')

    # mode = 'uniform'
    # I_sampled = probe.sample_images(mode=mode, Nimages=Nimages)
    # plot_image_table(imgs=I_sampled, output_dir=output_dir, file_name='Uniform_SampledImage')
    #
    # mode = 'proper_using_pi'
    # I_sampled = probe.sample_images(mode=mode, Nimages=Nimages)
    # plot_image_table(imgs=I_sampled, output_dir=output_dir, file_name='ProperUsingPi_SampledImage')
    #
    # # TODO: Figure out the memory issues to include 'proper_using_final_pi' for both model
    #
    # mode = 'proper_using_final_pi'
    # shared_dat = theano.shared(mnist.dtrain)
    # N, Cin, h, w = np.shape(mnist.dtrain)
    # n_batches = N / probe.model.batch_size
    # index = T.iscalar()
    #
    # update_after_train = theano.function([index, probe.model.is_train, probe.model.momentum_bn], [],
    #                                                                             updates=probe.model.updates_after_train, on_unused_input='warn',
    #                                                                             givens={probe.model.x: shared_dat[index * probe.model.batch_size: (index + 1) * probe.model.batch_size],
    #                                                                                     probe.model.y: shared_dat[index * probe.model.batch_size: (index + 1) * probe.model.batch_size]})
    #
    # for minibatch_index in xrange(n_batches):
    #     update_after_train(minibatch_index, 0, 1.0)
    #
    # I_sampled = probe.sample_images(mode=mode, Nimages=Nimages, top_down_mode=probe.model.top_down_mode, is_TD_relu=probe.model.is_TD_relu)
    # plot_image_table(imgs=I_sampled, output_dir=output_dir, file_name='ProperUsingPiFinal_SampledImage')
    #
    # mode = 'use_ta_BU'
    # I_sampled = probe.sample_images(mode=mode, Nimages=Nimages, input=dat, top_down_mode=probe.model.top_down_mode, is_TD_relu=probe.model.is_TD_relu)
    # plot_image_table(imgs=I_sampled, output_dir=output_dir, file_name='UsingPiBU_SampledImage')

if __name__ == '__main__':
    model_file_name = 'model_best.zip'
    training_name = 'MNIST_Conv_Small_5_Layers_nofactor_semi_sup_Ni60000_Nlabel100_b100_lr_init_0_2_lr_final_0_0001_nepoch_500_reluBU_normal_simple_sample_1hot_zeroSW_signcost_1_0_rec_0_2_KL_1_0_20170120_222303'
    data_mode = 'MNIST'
    runProbe(model_file_name=model_file_name, training_name=training_name, data_mode=data_mode)





