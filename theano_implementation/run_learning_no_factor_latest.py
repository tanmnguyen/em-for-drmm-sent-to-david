__author__ = 'tannguyen'

import matplotlib as mpl
mpl.use('Agg')

import os
import sys
import datetime
from useful_models_latest import *

from load_data_latest import DATA
from train_no_factor_latest import TrainNoFactorDRM
import numpy as np

if __name__ == '__main__':
    model_name = 'CIFAR10_Conv_Large_9_Layers'  # specify the model to use
    train_mode = 'semisupervised'  # choose the train mode: semisupervised, supervised or unsupervised

    init_params = False  # use specific param values to initialize the model
    debug_mode = False  # if True, run the code in debug_mode
    xavier_init = True  # use Xavier initialization

    num_epoch_to_plot_NLL = 10  # plot negative log-likelihood at each epoch
    n_monitor_batches = 10  # use this number of batches to monitor the training

    is_bn_BU = True  # use BatchNorm in the Bottom Up (BU)
    is_bn_TD = False  # use BatchNorm in the Top Down (TD)
    is_tied_bn = False  # tie the params of BatchNorm in BU and TD
    denoising = 'simple'  # choose the denoising mode: simple, linear
    top_down_mode = 'normal'  # {signed, normal} use signed DRMM or NN-DRMM
    is_TD_relu = False  # {True, False} use ReLU in the TopDown or not
    is_reluI = False  # {True, False} if True, apply ReLU on the reconstructed image
    is_end_to_end = True  # include Softmax Regression in TopDown
    is_sample_c = True  # {True, False} if True, do sampling during reconstruction
    is_one_hot = True  # {True, False} if True, turn c_hat into a one hot encoding vector and use it to reconstruct the image
    nonlin = 'relu'  # choose the non-linearity for the BU: relu, abs, tanh,...
    is_Dg = False  # alternative for BatchNorm derived from the DRM
    train_method = 'SGD'  # {SGD, adam} training method
    is_prun = False
    prun_threshold = 0.0

    seed = 2  # set random seed
    Nlabeled = 8000  # number of labeled examples used during training

    num_TD_samplings = 1  # number of samples used in the Top-Down reconstruction

    KL_coef = 0.5  # weight of the KL divergence cost
    KL_t_coef = 0.0  # weight of the KL divergence on t cost. We are not using it now
    sign_cost_weight = 0.5  # weight of hte sign cost
    lr_init = 0.2  # initial learning rate
    lr_final = 0.0001  # final learning rate
    batch_size = 50  # number of examples sent to the model in each iteration
    max_epochs = 500  # TODO: warning - max_epoch is used for update the learning rate at each epoch and to stop the training
    # We need have separate parameters for each of them.

    noise_std = 0.6  # std of noise added to each layer during training

    grad_min = -np.inf  # grad_min and grad_max used to clip the gradients
    grad_max = np.inf

    name_folder = 'Train'

    np.random.seed(seed)

    if model_name.upper().startswith('MNIST'):
        print(model_name)
        preprocess_mode = False  # not preprocess data
        Cin = 1  # number of channels of the input
        H = 28  # height of input images
        W = 28  # width of input images
        Ni = 60000  # total number of training examples
        reconst_weights = [0.2, 0.0, 0.0, 0.0, 0.0]  # set reconstruction weights
        noise_weights = [0.0, 0.0, 0.0, 0.0, 0.0]  # set weights for noise at each layer
    elif model_name.upper().startswith('CIFAR10') or model_name.upper().startswith('BLENDER'):
        print(model_name)
        preprocess_mode = True  # WARN: THIS SHOULD BE ALWAYS ON FOR CIFAR10
        Cin = 3  # number of channels of the input
        H = 32  # height of input images
        W = 32  # width of input images
        Ni = 50000  # total number of training examples
        reconst_weights = [0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]  # set reconstruction weights
        noise_weights = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]  # set weights for noise at each layer
    elif model_name.upper().startswith('SVHN'):
        print(model_name)
        preprocess_mode = True  # WARN: THIS SHOULD BE ALWAYS ON FOR CIFAR10
        Cin = 3  # number of channels of the input
        H = 32  # height of input images
        W = 32  # width of input images
        Ni = 73257  # total number of training examples
        reconst_weights = [0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]  # set reconstruction weights
        noise_weights = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]  # set weights for noise at each layer
    else:
        print('Please specify your model')
        raise

    #############################
    # No touch#
    stop_mode = 'NLL'
    tol = 1e-10
    verbose = True
    lr_decay = 0.01
    epoch_to_reduce_lr = (100, 200)
    #############################

    # Set up the folder to save the results
    timestamp = datetime.datetime.now
    result_folder_name = 'CIFAR10_Conv_Large_9_Layers_nofactor_semi_sup_Ni50000_Nlabel8000_b100_lr_init_0_2_lr_final_0_0001_nepoch_500_reluBU_normal_simple_sample_1hot_signcost_0_5_rec_0_5_KL_0_5_{: % Y % m % d_ % H % M % S}'.format(datetime.datetime.now())

    # change this output directory to your local directory
    output_dir = os.path.join('./', result_folder_name)

    param_dir = os.path.join(output_dir, 'Train', 'params', 'EM_results_epoch_25.pkl')
    # output_dir = '/Users/heatherseeba/Documents/research_results/EM_results'
    # data_dir = '/Users/heatherseeba/repos/em_drm/data/cifar10'

    # Start running
    if model_name == 'CIFAR10_Conv_Large_9_Layers' or model_name == 'BLENDER_Conv_Large_9_Layers' \
            or model_name == 'REVERSIBLE_CIFAR10_Conv_Large_9_Layers':

        if (train_mode == 'semisupervised' or train_mode == 'semisupervised_MSMP'
            or train_mode == 'semisupervised_MSMP_reconst_layer' or train_mode == 'semisupervised_MSMP_Tau'
            or train_mode == 'semisupervised_MSMP_MultiSup' or train_mode == 'semisupervised_MultiSup') \
                and Nlabeled != 50000:
            print('data_mode: semisupervised')
            data_mode = 'semisupervised'
        else:
            print('data_mode: all')
            data_mode = 'all'

        if model_name == 'CIFAR10_Conv_Large_9_Layers' or model_name == 'BLENDER_Conv_Large_9_Layers' \
                or model_name == 'REVERSIBLE_CIFAR10_Conv_Large_9_Layers':
            if (train_mode == 'semisupervised' or train_mode == 'semisupervised_MSMP'
                or train_mode == 'semisupervised_MSMP_reconst_layer' or train_mode == 'semisupervised_MSMP_Tau'
                or train_mode == 'semisupervised_MSMP_MultiSup' or train_mode == 'semisupervised_MultiSup') \
                    and Nlabeled != 50000:
                print('data_mode: semisupervised')
                data_mode = 'semisupervised'
            else:
                print('data_mode: all')
                data_mode = 'all'

            if model_name == 'CIFAR10_Conv_Large_9_Layers':
                data_dir = '../data/cifar10'
                cifar10 = DATA(dataset_name='CIFAR10', data_mode=data_mode, data_dir=data_dir, Nlabeled=Nlabeled, Ni=Ni,
                               Cin=Cin, H=H, W=W, seed=seed, preprocess=preprocess_mode)
            elif model_name == 'BLENDER_Conv_Large_9_Layers':
                data_dir = '/mnt/data/'
                cifar10 = DATA(dataset_name='REVERSIBLE_BLENDER_2AXIS', data_mode=data_mode, data_dir=data_dir,
                               Nlabeled=Nlabeled, Ni=Ni,
                               Cin=Cin, H=H, W=W, seed=seed, preprocess=preprocess_mode)
            elif model_name == 'REVERSIBLE_CIFAR10_Conv_Large_9_Layers':
                data_dir = '/mnt/data/'
                cifar10 = DATA(dataset_name='REVERSIBLE_CIFAR10', data_mode=data_mode, data_dir=data_dir,
                               Nlabeled=Nlabeled, Ni=Ni,
                               Cin=Cin, H=H, W=W, seed=seed, preprocess=preprocess_mode)
            else:
                print('Your model is wrong')
                raise

        if model_name == 'CIFAR10_Conv_Large_9_Layers' or model_name == 'BLENDER_Conv_Large_9_Layers' \
                or model_name == 'REVERSIBLE_CIFAR10_Conv_Large_9_Layers':
            cifar10_model = CIFAR10_Conv_Large_9_Layers(batch_size=batch_size, Cin=Cin, W=W, H=H,
                                                        seed=seed, param_dir=param_dir,
                                                        train_mode=train_mode,
                                                        reconst_weights=reconst_weights, xavier_init=xavier_init,
                                                        grad_min=grad_min, grad_max=grad_max, init_params=init_params,
                                                        denoising=denoising, noise_std=noise_std, noise_weights=noise_weights,
                                                        is_bn_BU=is_bn_BU, is_bn_TD=is_bn_TD, is_tied_bn=is_tied_bn,
                                                        top_down_mode=top_down_mode, is_TD_relu=is_TD_relu, nonlin=nonlin,
                                                        is_Dg=is_Dg, method=train_method, KL_coef=KL_coef,
                                                        is_reluI=is_reluI, is_end_to_end=is_end_to_end,
                                                        sign_cost_weight=sign_cost_weight, is_sample_c=is_sample_c,
                                                        is_one_hot=is_one_hot, KL_t_coef=KL_t_coef,
                                                        num_TD_samplings=num_TD_samplings, is_prun=is_prun, prun_threshold=prun_threshold)

        else:
            print('Please build your model in train_model_no_factor_latest.py')
            raise

        train_cifar10_model = TrainNoFactorDRM(batch_size=batch_size, trainx_unl=cifar10.trainx_unl,
                                             trainx_lab=cifar10.trainx_lab, testx=cifar10.testx,
                                             validx=cifar10.validx,
                                             trainy_lab=cifar10.trainy_lab, testy=cifar10.testy,
                                             validy=cifar10.validy,
                                             model=cifar10_model,
                                             output_dir=os.path.join(output_dir, name_folder),
                                             seed=seed)

        # run training
        if train_mode == 'supervised':
            train_cifar10_model.train_supervised(max_epochs=max_epochs, verbose=verbose, tol=tol, stop_mode=stop_mode,
                                                 lr_init=lr_init, epoch_to_reduce_lr=epoch_to_reduce_lr,
                                                 lr_decay=lr_decay, lr_final=lr_final,
                                                 plot_NLL_every_epoch=num_epoch_to_plot_NLL, debug_mode=debug_mode)
        elif train_mode == 'unsupervised' or train_mode == 'unsupervised_MSMP':
            train_cifar10_model.train_unsupervised(max_epochs=max_epochs, verbose=verbose, tol=tol, stop_mode=stop_mode,
                                                 lr_init=lr_init, epoch_to_reduce_lr=epoch_to_reduce_lr,
                                                 lr_decay=lr_decay, lr_final=lr_final,
                                                 debug_mode=debug_mode)
        elif train_mode == 'semisupervised' or train_mode == 'semisupervised_MSMP' or train_mode == 'semisupervised_MSMP_reconst_layer' \
                or train_mode == 'semisupervised_MSMP_Tau' or train_mode == 'semisupervised_MSMP_MultiSup' \
                or train_mode == 'semisupervised_MultiSup':
            train_cifar10_model.train_semisupervised(max_epochs=max_epochs, verbose=verbose, tol=tol, stop_mode=stop_mode,
                                                     lr_init=lr_init, epoch_to_reduce_lr=epoch_to_reduce_lr,
                                                     lr_decay=lr_decay, lr_final=lr_final,
                                                     num_epoch_to_plot_NLL=num_epoch_to_plot_NLL, debug_mode=debug_mode)

        else:
            train_cifar10_model.output_dir = os.path.join(output_dir, 'Finetune_end_to_end_using_50K_labeled_data')
            if not os.path.exists(train_cifar10_model.output_dir):
                os.makedirs(train_cifar10_model.output_dir)

            train_cifar10_model.train_supervised(max_epochs=max_epochs, verbose=verbose, tol=tol, stop_mode=stop_mode,
                                                 lr_init=lr_init, epoch_to_reduce_lr=epoch_to_reduce_lr,
                                                 lr_decay=lr_decay, lr_final=lr_final,
                                                 plot_NLL_every_epoch=num_epoch_to_plot_NLL, debug_mode=debug_mode)

    elif model_name == 'MNIST_Conv_Small_5_Layers':
        data_dir = '../data/mnist.npz'

        # set the data_mode: all, semisupervised, small
        if (train_mode == 'semisupervised' or train_mode == 'semisupervised_MSMP'
            or train_mode == 'semisupervised_MSMP_reconst_layer' or train_mode == 'semisupervised_MSMP_Tau'
            or train_mode == 'semisupervised_MSMP_MultiSup' or train_mode == 'semisupervised_MultiSup') \
                and Nlabeled != 50000:
            print('data_mode: semisupervised')
            data_mode = 'semisupervised'
        else:
            print('data_mode: all')
            data_mode = 'all'

        # load data
        mnist = DATA(dataset_name='MNIST', data_mode=data_mode, data_dir=data_dir, Nlabeled=Nlabeled, Ni=Ni, Cin=Cin,
                     H=H, W=W, seed=seed, preprocess=preprocess_mode)

        # load models
        if model_name == 'MNIST_Conv_Small_5_Layers':
            mnist_model = MNIST_Conv_Small_5_Layers(batch_size=batch_size, Cin=Cin, W=W, H=H,
                                                    seed=seed, param_dir=param_dir,
                                                    train_mode=train_mode,
                                                    reconst_weights=reconst_weights, xavier_init=xavier_init, grad_min=grad_min,
                                                    grad_max=grad_max,
                                                    init_params=init_params, denoising=denoising, noise_std=noise_std, noise_weights=noise_weights,
                                                    is_bn_BU=is_bn_BU, is_bn_TD=is_bn_TD, is_tied_bn=is_tied_bn,
                                                    top_down_mode=top_down_mode, is_TD_relu=is_TD_relu, nonlin=nonlin,
                                                    is_Dg=is_Dg, method=train_method, KL_coef=KL_coef, sign_cost_weight=sign_cost_weight,
                                                    is_reluI=is_reluI, is_end_to_end=is_end_to_end,
                                                    is_sample_c=is_sample_c, is_one_hot=is_one_hot, num_TD_samplings=num_TD_samplings,
                                                    KL_t_coef=KL_t_coef, is_prun=is_prun, prun_threshold=prun_threshold)

        else:
            print('Please build your model in train_model_no_factor_latest.py')
            raise

        # initiate the training object
        train_mnist_model = TrainNoFactorDRM(batch_size=batch_size, trainx_unl=mnist.trainx_unl,
                                             trainx_lab=mnist.trainx_lab, testx=mnist.testx,
                                             validx=mnist.validx,
                                             trainy_lab=mnist.trainy_lab, testy=mnist.testy,
                                             validy=mnist.validy,
                                             model=mnist_model,
                                             output_dir=os.path.join(output_dir, name_folder),
                                             seed=seed)

        # run training
        if train_mode == 'supervised':
            train_mnist_model.train_supervised(max_epochs=max_epochs, verbose=verbose, tol=tol, stop_mode=stop_mode,
                                               lr_init=lr_init, epoch_to_reduce_lr=epoch_to_reduce_lr, lr_decay=lr_decay, lr_final=lr_final,
                                               plot_NLL_every_epoch=num_epoch_to_plot_NLL, debug_mode=debug_mode)
        elif train_mode == 'unsupervised' or train_mode == 'unsupervised_MSMP':
            train_mnist_model.train_unsupervised(max_epochs=max_epochs, verbose=verbose, tol=tol, stop_mode=stop_mode,
                                                   lr_init=lr_init, epoch_to_reduce_lr=epoch_to_reduce_lr, lr_decay=lr_decay, lr_final=lr_final,
                                                   debug_mode=debug_mode)
        elif train_mode == 'semisupervised' or train_mode == 'semisupervised_MSMP' or train_mode == 'semisupervised_MSMP_reconst_layer'\
                or train_mode == 'semisupervised_MSMP_Tau' or train_mode == 'semisupervised_MSMP_MultiSup' \
                or train_mode == 'semisupervised_MultiSup':
            train_mnist_model.train_semisupervised(max_epochs=max_epochs, verbose=verbose, tol=tol, stop_mode=stop_mode,
                                                   lr_init=lr_init, epoch_to_reduce_lr=epoch_to_reduce_lr, lr_decay=lr_decay, lr_final=lr_final,
                                                   num_epoch_to_plot_NLL=num_epoch_to_plot_NLL, debug_mode=debug_mode)

        else:
            train_mnist_model.output_dir=os.path.join(output_dir, 'Finetune_end_to_end_using_50K_labeled_data')
            if not os.path.exists(train_mnist_model.output_dir):
                os.makedirs(train_mnist_model.output_dir)

            train_mnist_model.train_supervised(max_epochs=max_epochs, verbose=verbose, tol=tol, stop_mode=stop_mode,
                                               lr_init=lr_init, epoch_to_reduce_lr=epoch_to_reduce_lr, lr_decay=lr_decay, lr_final=lr_final,
                                               plot_NLL_every_epoch=num_epoch_to_plot_NLL, debug_mode=debug_mode)
    elif model_name == 'SVHN_Conv_Large_9_Layers':
        data_dir = '../data/'

        if (train_mode == 'semisupervised' or train_mode == 'semisupervised_MSMP'
            or train_mode == 'semisupervised_MSMP_reconst_layer' or train_mode == 'semisupervised_MSMP_Tau'
            or train_mode == 'semisupervised_MSMP_MultiSup' or train_mode == 'semisupervised_MultiSup') \
                and Nlabeled != 50000:
            print('data_mode: semisupervised')
            data_mode = 'semisupervised'
        else:
            print('data_mode: all')
            data_mode = 'all'

        svhn = DATA(dataset_name='SVHN', data_mode=data_mode, data_dir=data_dir, Nlabeled=Nlabeled, Ni=Ni,
                    Cin=Cin, H=H, W=W, seed=seed, preprocess=preprocess_mode)

        if model_name == 'SVHN_Conv_Large_9_Layers':
            svhn_model = CIFAR10_Conv_Large_9_Layers(batch_size=batch_size, Cin=Cin, W=W, H=H,
                                                     seed=seed, param_dir=param_dir,
                                                     train_mode=train_mode,
                                                     reconst_weights=reconst_weights,
                                                     xavier_init=xavier_init,
                                                     grad_min=grad_min, grad_max=grad_max,
                                                     init_params=init_params,
                                                     denoising=denoising, noise_std=noise_std,
                                                     noise_weights=noise_weights,
                                                     is_bn_BU=is_bn_BU, is_bn_TD=is_bn_TD,
                                                     is_tied_bn=is_tied_bn,
                                                     top_down_mode=top_down_mode, is_TD_relu=is_TD_relu,
                                                     nonlin=nonlin,
                                                     is_Dg=is_Dg, method=train_method, KL_coef=KL_coef,
                                                     is_reluI=is_reluI, is_end_to_end=is_end_to_end,
                                                     sign_cost_weight=sign_cost_weight,
                                                     is_sample_c=is_sample_c,
                                                     is_one_hot=is_one_hot,
                                                     is_prun=is_prun, prun_threshold=prun_threshold)

        else:
            print('Please build your model in train_model_no_factor_latest.py')
            raise

        train_svhn_model = TrainNoFactorDRM(batch_size=batch_size, trainx_unl=svhn.trainx_unl,
                                            trainx_lab=svhn.trainx_lab, testx=svhn.testx,
                                            validx=svhn.validx,
                                            trainy_lab=svhn.trainy_lab, testy=svhn.testy,
                                            validy=svhn.validy,
                                            model=svhn_model,
                                            output_dir=os.path.join(output_dir, name_folder),
                                            seed=seed)

        # run training
        if train_mode == 'supervised':
            train_svhn_model.train_supervised(max_epochs=max_epochs, verbose=verbose, tol=tol,
                                              stop_mode=stop_mode,
                                              lr_init=lr_init, epoch_to_reduce_lr=epoch_to_reduce_lr,
                                              lr_decay=lr_decay, lr_final=lr_final,
                                              plot_NLL_every_epoch=num_epoch_to_plot_NLL,
                                              debug_mode=debug_mode)
        elif train_mode == 'unsupervised' or train_mode == 'unsupervised_MSMP':
            train_svhn_model.train_unsupervised(max_epochs=max_epochs, verbose=verbose, tol=tol,
                                                stop_mode=stop_mode,
                                                lr_init=lr_init, epoch_to_reduce_lr=epoch_to_reduce_lr,
                                                lr_decay=lr_decay, lr_final=lr_final,
                                                debug_mode=debug_mode)
        elif train_mode == 'semisupervised' or train_mode == 'semisupervised_MSMP' or train_mode == 'semisupervised_MSMP_reconst_layer' \
                or train_mode == 'semisupervised_MSMP_Tau' or train_mode == 'semisupervised_MSMP_MultiSup' \
                or train_mode == 'semisupervised_MultiSup':
            train_svhn_model.train_semisupervised(max_epochs=max_epochs, verbose=verbose, tol=tol,
                                                  stop_mode=stop_mode,
                                                  lr_init=lr_init, epoch_to_reduce_lr=epoch_to_reduce_lr,
                                                  lr_decay=lr_decay, lr_final=lr_final,
                                                  num_epoch_to_plot_NLL=num_epoch_to_plot_NLL,
                                                  debug_mode=debug_mode)

        else:
            train_svhn_model.output_dir = os.path.join(output_dir,
                                                       'Finetune_end_to_end_using_50K_labeled_data')
            if not os.path.exists(train_svhn_model.output_dir):
                os.makedirs(train_svhn_model.output_dir)

            train_svhn_model.train_supervised(max_epochs=max_epochs, verbose=verbose, tol=tol,
                                              stop_mode=stop_mode,
                                              lr_init=lr_init, epoch_to_reduce_lr=epoch_to_reduce_lr,
                                              lr_decay=lr_decay, lr_final=lr_final,
                                              plot_NLL_every_epoch=num_epoch_to_plot_NLL,
                                              debug_mode=debug_mode)


                # except KeyboardInterrupt:
    #     print('Training stopped manually by user')

    # finally:
    #     print('Plot reconstruction and send the results to s3')
    #     try:
    #         import analyze_DRM_latest
    #
    #         if model_name.upper().startswith('MNIST'):
    #             analyze_DRM_latest.runProbe(model_file_name='model_best.zip', training_name=result_folder_name, data_mode='MNIST')
    #             analyze_DRM_latest.runProbe(model_file_name='model_latest.zip', training_name=result_folder_name, data_mode='MNIST')
    #         elif model_name.upper().startswith('CIFAR10'):
    #             analyze_DRM_latest.runProbe(model_file_name='model_best.zip', training_name=result_folder_name,
    #                                         data_mode='CIFAR10')
    #             analyze_DRM_latest.runProbe(model_file_name='model_latest.zip', training_name=result_folder_name,
    #                                         data_mode='CIFAR10')
    #         else:
    #             print('Please specify the analyze code for your model in analyze_DRM_latest.py')
    #     except:
    #         print('ERROR: analysis fails - fix analyze_DRM_latest')
    #         print('Unexpected error:', sys.exc_info()[0])
    #         raise
    #     finally:
    #         os.system('aws s3 sync /home/ubuntu/research_results/EM_results s3://drm-em-results --region us-west-2 > s3sync.log')



